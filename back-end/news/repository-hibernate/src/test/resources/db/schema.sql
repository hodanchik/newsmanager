DROP TABLE IF EXISTS author CASCADE;
CREATE TABLE author
(
    id      bigint         NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name    varchar(30) NOT NULL,
    surname varchar(30) NOT NULL,
    PRIMARY KEY (id)
);
DROP TABLE IF EXISTS news CASCADE;
CREATE TABLE news
(
    id                bigint           NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title             varchar(30)   NOT NULL,
    short_text        varchar(100)  NOT NULL,
    full_text         varchar(2000) NOT NULL,
    creation_date     date          NOT NULL,
    modification_date date          NOT NULL,
    PRIMARY KEY (id)
);
DROP TABLE IF EXISTS tag CASCADE;
CREATE TABLE tag
(
    id   bigint         NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name varchar(30) NOT NULL,
    PRIMARY KEY (id)
);
DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE users
(
    id       bigint         NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name     varchar(20) NOT NULL,
    surname  varchar(20) NOT NULL,
    login    varchar(30) NOT NULL,
    password varchar(30) NOT NULL,
    PRIMARY KEY (id)
);
DROP TABLE IF EXISTS roles CASCADE;
CREATE TABLE roles
(
    user_id   bigint         NOT NULL,
    role_name varchar(30) NOT NULL,
    constraint fk_roles_users_id
        foreign key (user_id) references users (id)
);
DROP TABLE IF EXISTS news_author CASCADE;
CREATE TABLE news_author
(
    news_id   bigint NOT NULL,
    author_id bigint NOT NULL,
    primary key (news_id, author_id),
    constraint fk_news_author_news_id
        foreign key (news_id) references news (id) ,
    constraint fk_news_author_author_id
        foreign key (author_id) references author (id)
);

DROP TABLE IF EXISTS news_tag CASCADE;
CREATE TABLE news_tag
(
    news_id   bigint NOT NULL,
    tag_id bigint NOT NULL,
    constraint fk_news_tag_news_id
        foreign key (news_id) references news (id) ,
    constraint fk_news_tag_tag_id
        foreign key (tag_id) references tag (id)
);


