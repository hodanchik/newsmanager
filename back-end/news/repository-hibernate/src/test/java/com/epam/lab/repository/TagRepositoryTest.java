package com.epam.lab.repository;

import com.epam.lab.configuration.RepositoryConfiguration;
import com.epam.lab.exception.NotFoundByIdException;
import com.epam.lab.model.Tag;
import com.epam.lab.specification.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.List;


@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RepositoryConfiguration.class})
@ActiveProfiles("dev")
@Transactional
public class TagRepositoryTest extends AuthorRepositoryImpl {
    SearchSpecification specifications;

    @Autowired
    TagRepository tagRepository;

    @Before
    public void setUp() {
       SearchCriteria searchCriteria = new SearchCriteria("name", ":", "tag-test09");
        specifications = new NewsSearchSpecification(searchCriteria);
    }

    @Test
    public void testFindOne() {
        Tag tagOne = tagRepository.findOne(1);
        Tag expectedTag = new Tag(1, "tag-test01");
        Assert.assertEquals(expectedTag, tagOne);
    }
    @Test(expected = NotFoundByIdException.class)
    public void testFindOneWithException() {
        tagRepository.findOne(100000);
    }

    @Test
    public void testFindByName() {
        List<Tag> tags = tagRepository.findByName("tag-test01");
        Tag expectedTag = new Tag(1, "tag-test01");
        Assert.assertEquals(expectedTag, tags.get(0));
    }

    @Test
    public void testSave() {
        Tag tag = new Tag();
        tag.setName("tag-test-for-ave");
        long save = tagRepository.save(tag);
        Assert.assertEquals(21, save);
    }

    @Test
    public void testDeleteSuccess() {
        Assert.assertTrue(tagRepository.delete(2));
    }

    @Test(expected = NotFoundByIdException.class)
    public void testDeleteUnSuccess() {
        Assert.assertTrue(tagRepository.delete(2999));
    }

    @Test
    public void testUpdate() {
        System.out.println(tagRepository.getClass());
        Tag tagUpdate = new Tag(1,"update");
        tagRepository.update(tagUpdate);
        Tag tagAfterUpdate = tagRepository.findOne(1);
        Assert.assertEquals(tagUpdate, tagAfterUpdate);
    }
    @Test
    public void testGetAll() {
        List<Tag> authorList = tagRepository.findAll();
        int expectedSize = 20;
        Assert.assertEquals(expectedSize, authorList.size());
    }

    @Test
    public void testFind() {
        List<Tag> tags = tagRepository.find(specifications);
        Tag expectedTag = new Tag(9, "tag-test09");
        Assert.assertEquals(expectedTag, tags.get(0));
    }

}
