package com.epam.lab.repository;

import com.epam.lab.configuration.RepositoryConfiguration;
import com.epam.lab.exception.NotFoundByIdException;
import com.epam.lab.model.Author;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertEquals;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RepositoryConfiguration.class})
@ActiveProfiles("dev")
@Transactional
public class AuthorRepositoryTest {
private Author expectedAuthor = new Author(1, "Name-test01", "Surname-test01");
    @Autowired
    private AuthorRepository authorRepository;


    @Test
    public void testFindOne() {
        Author authorOne = authorRepository.findOne(1);
        Author expectedAuthor = new Author(1, "Name-test01", "Surname-test01");
        assertEquals(expectedAuthor, authorOne);
    }

    @Test(expected = NotFoundByIdException.class)
    public void testFindOneWithException() {
        authorRepository.findOne(100000);
    }

    @Test
    public void shouldGetOne() {
        Author actual = authorRepository.findOne(1);
        assertEquals(expectedAuthor, actual);
    }
    @Test
    public void testSave() {
        Author author = new Author();
        author.setName("test");
        author.setSurname("saveTester");
        long save = authorRepository.save(author);
        assertEquals(21, save);
    }

    @Test
    public void testDeleteSuccess() {
         Assert.assertTrue(authorRepository.delete(2));
    }

    @Test(expected = NotFoundByIdException.class)
    public void testDeleteUnSuccess() {
        Assert.assertTrue(authorRepository.delete(2999));
    }
    @Test
    public void testUpdate() {
        String authorNameBeforeUpdate = authorRepository.findOne(1).getName();
        Author authorUpdate = new Author(1,"update", "update");
        authorRepository.update(authorUpdate);
        String authorNameAfterUpdate = authorRepository.findOne(1).getName();
        Assert.assertNotEquals(authorNameBeforeUpdate, authorNameAfterUpdate);
    }
    @Test
    public void testGetAll() {
        List<Author> authorList = authorRepository.findAll();
        int expectedSize = 20;
        assertEquals(expectedSize, authorList.size());
    }
}