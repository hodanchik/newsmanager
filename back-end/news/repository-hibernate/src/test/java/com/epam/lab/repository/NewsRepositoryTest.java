package com.epam.lab.repository;

import com.epam.lab.configuration.RepositoryConfiguration;
import com.epam.lab.exception.NotFoundByIdException;
import com.epam.lab.exception.RepositoryException;
import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.specification.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static org.junit.Assert.assertTrue;

//
//@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {RepositoryConfiguration.class})
//@ActiveProfiles("dev")
//@Transactional
public class NewsRepositoryTest {
//    List<SortSpecification<News>> sortSpecificationsList;
//    SortSpecification<News> sortSpecificationForAuthorName;
//    SearchSpecification specifications;
//    SearchSpecification specificationWithException;
//    SortSpecification sortSpecificationByTittleAsc;
//    SortCriteria SortCriteriaByTittleAsc;
//    SearchCriteria searchCriteriaForSurname;
//    List<SortSpecification<News>> sortCriteriaListForFindNews;
//    List<SortSpecification<News>> sortCriteriaListForFindNewsByTittle;
//    News expectedFirstNewsSortedByTittle;
//    @Autowired
//    NewsRepository newsRepository;
//    private News newsForSelect;
//    private Author authorForSelect;
//    private Set<Tag> listTagForSelect;
//
//
//    @Before
//    public void setUp() {
//        SortCriteria sortCriteria = new SortCriteria("author_name", "desc");
//        sortSpecificationForAuthorName = new NewsSortSpecification(sortCriteria);
//        SearchCriteria searchCriteriaWithExcept = new SearchCriteria("aut_name_Eion", ":","value");
//        specificationWithException = new NewsSearchSpecification(searchCriteriaWithExcept);
//        sortSpecificationsList = new ArrayList<>();
//        sortSpecificationsList.add(sortSpecificationForAuthorName);
//        SearchCriteria searchCriteria = new SearchCriteria("author_name", ":", "Name-test01");
//        specifications = new NewsSearchSpecification(searchCriteria);
//        sortCriteriaListForFindNews = new ArrayList<>();
//        sortCriteriaListForFindNewsByTittle = new ArrayList<>();
//        SortCriteriaByTittleAsc = new SortCriteria("title", "asc");
//        sortSpecificationByTittleAsc = new NewsSortSpecification(SortCriteriaByTittleAsc);
//        sortCriteriaListForFindNewsByTittle.add(sortSpecificationByTittleAsc);
//        searchCriteriaForSurname = new SearchCriteria("", "", "");
//        searchCriteriaForSurname.setKey("author_surname");
//        searchCriteriaForSurname.setOperation(":");
//        searchCriteriaForSurname.setValue("Surname-test01");
//
//        listTagForSelect = new HashSet<>();
//        listTagForSelect.add(new Tag(1, "tag-test01"));
//        listTagForSelect.add(new Tag(2, "tag-test02"));
//        authorForSelect = new Author(1, "Name-test01", "Surname-test01");
//        newsForSelect = new News(1, "Title-test01", "Short text in test01", "Some text of news in test01",
//                LocalDate.of(2020, 01, 28), LocalDate.of(2020, 01, 28),
//                listTagForSelect, authorForSelect);
//        expectedFirstNewsSortedByTittle = new News(1, "Title-test01", "Short text in test01", "Some text of news in test01",
//                LocalDate.of(2020, 01, 28), LocalDate.of(2020, 01, 28),
//                listTagForSelect, authorForSelect);
//    }
//
//    @Test
//    public void testFindOne() {
//        News newsOne = newsRepository.findOne(1);
//        Assert.assertEquals(newsForSelect, newsOne);
//    }
//
//    @Test(expected = NotFoundByIdException.class)
//    public void testFindOneWithException() {
//        newsRepository.findOne(100000);
//    }
//
//    @Test
//    public void testSave() {
//        Author authorUpdate = new Author(1, "Name-test01", "Surname-test01");
//        Set<Tag> tagSet = new HashSet<>();
//        tagSet.add(new Tag(1, "test"));
//        tagSet.add(new Tag(2, "two"));
//        tagSet.add(new Tag(3, "three"));
//        News news = new News("title-test", "Short text in test",
//                "Some Full_text of news in test", LocalDate.of(2020, 2, 28),
//                LocalDate.of(2020, 2, 28), tagSet, authorUpdate);
//        long saveId = newsRepository.save(news);
//        long expectedId = 27;
//        Assert.assertEquals(expectedId, saveId);
//    }
//
//    @Test
//    public void testDeleteSuccess() {
//        assertTrue(newsRepository.delete(2));
//    }
//
//    @Test(expected = NotFoundByIdException.class)
//    public void testDeleteUnSuccess() {
//        Assert.assertTrue(newsRepository.delete(2999));
//    }
//    @Test
//    public void testUpdate() {
//
//        Author authorUpdate = new Author(3, "Name-test01", "Surname-test01");
//        Set<Tag> tags = new HashSet<>();
//        tags.add(new Tag(1, "test"));
//        tags.add(new Tag(5, "two"));
//        tags.add(new Tag(3, "three"));
//        News newsUpdate = new News(1, "title-test", "Short text in test",
//                "Some Full_text of news in test", LocalDate.of(2020, 2, 28),
//                LocalDate.of(2020, 2, 28), tags, authorUpdate);
//        newsRepository.update(newsUpdate);
//        News newsAfterUpdate = newsRepository.findOne(1);
//        long expectAuthorId = 3;
//        Assert.assertEquals(expectAuthorId, newsAfterUpdate.getAuthor().getId());
//
//    }
//
//
//    @Test
//    public void findAll() {
//        List<News> allNewsList = newsRepository.findAll();
//        int expectedSize = 26;
//        int actualSize = allNewsList.size();
//        Assert.assertEquals(expectedSize, actualSize);
//    }
//
//    @Test
//    public void findAllWithSort() {
//        List<News> allNewsList = newsRepository.findAllWithSort(sortSpecificationsList);
//        Set<Tag> tagsSet = new HashSet<>();
//        tagsSet.add(new Tag(18, "tag-test18"));
//        News expectedNews = new News(21, "Title-test21", "Short text in test21",
//                "Some text of news in test21", LocalDate.of(2020, 1, 28),
//                LocalDate.of(2020, 1, 28),
//                tagsSet, new Author(20, "Name-test20", "Surname-test20"));
//        Assert.assertEquals(expectedNews, allNewsList.get(0));
//    }
//
//    @Test
//    public void findNewsByAuthorName() {
//        List<News> allNewsByAuthorNameList = newsRepository.findNews(specifications, sortCriteriaListForFindNews);
//        int actualSize = allNewsByAuthorNameList.size();
//        int expectedSize = 4;
//        Assert.assertEquals(expectedSize, actualSize);
//    }
//
//    @Test
//    public void findNewsByAuthorNameAndSort() {
//        List<News> allNewsByAuthorNameList = newsRepository.findNews(specifications, sortCriteriaListForFindNewsByTittle);
//        int actualSize = allNewsByAuthorNameList.size();
//        int expectedSize = 4;
//        Assert.assertEquals(expectedSize, actualSize);
//        String expectedFirstTitle = "Title-test01";
//        Assert.assertEquals(expectedFirstTitle, allNewsByAuthorNameList.get(0).getTitle());
//    }
//
//    @Test
//    public void findNewsByAuthorSurname() {
//        SearchSpecification specifications = new NewsSearchSpecification(searchCriteriaForSurname);
//        List<News> allNewsByAuthorSurnameList = newsRepository.findNews(specifications, sortCriteriaListForFindNews);
//        int actualSize = allNewsByAuthorSurnameList.size();
//        int expectedSize = 2;
//        Assert.assertEquals(expectedSize, actualSize);
//    }
//
//    @Test(expected = RepositoryException.class)
//    public void findNewsWithException() {
//        List<News> newsList = newsRepository.findNews(specificationWithException, sortCriteriaListForFindNews);
//    }
//
//    @Test
//    public void findAllWithSortByTitle() {
//        List<News> allNewsList = newsRepository.findAllWithSort(sortCriteriaListForFindNewsByTittle);
//        String expectedTittleName = "Title-test01";
//        Assert.assertEquals(expectedTittleName, allNewsList.get(0).getTitle());
//    }
//
//    @Test(expected = RepositoryException.class)
//    public void findAllWithSortException() {
//        List<SortSpecification<News>> sortSpecificationsList = new ArrayList<>();
//        SortCriteria SortCriteriaWithExcepton = new SortCriteria("ti_ex_cep_tion", "asc");
//        SortSpecification sortSpecificationWithException = new NewsSortSpecification(SortCriteriaWithExcepton);
//        sortSpecificationsList.add(sortSpecificationWithException);
//        newsRepository.findAllWithSort(sortSpecificationsList);
//    }

}
