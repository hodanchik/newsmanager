package com.epam.lab.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "news")
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "title")
    private String title;
    @Column(name = "short_text")
    private String shortText;
    @Column(name = "full_text")
    private String fullText;
    @Column(name = "creation_date", columnDefinition = "DATE")
    private LocalDate creationDate;
    @Column(name = "modification_date", columnDefinition = "DATE")
    private LocalDate modificationDate;

    @ManyToMany
    @JoinTable(
            name = "news_tag",
            joinColumns = @JoinColumn(name = "news_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id")
    )
    private Set<Tag> tags = new HashSet<>();


    @ManyToOne
    @JoinTable(
            name = "news_author",
            joinColumns = @JoinColumn(name = "news_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "author_id", referencedColumnName = "id")
    )
    private Author author;

    public News() {
    }

    public News(String title, String shortText, String fullText, LocalDate creationDate,
                LocalDate modificationDate, Set<Tag> tags, Author author) {
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
        this.tags = tags;
        this.author = author;
    }

    public News(long id, String title, String shortText, String fullText, LocalDate creationDate,
                LocalDate modificationDate, Set<Tag> tags, Author author) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
        this.tags = tags;
        this.author = author;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof News)) return false;
        News news = (News) o;
        return getId() == news.getId() &&
                getTitle().equals(news.getTitle()) &&
                getShortText().equals(news.getShortText()) &&
                getFullText().equals(news.getFullText()) &&
                getCreationDate().equals(news.getCreationDate()) &&
                getModificationDate().equals(news.getModificationDate()) &&
                getTags().equals(news.getTags()) &&
                getAuthor().equals(news.getAuthor());
    }


    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getShortText(), getFullText(), getCreationDate(), getModificationDate(), getTags(), getAuthor());
    }

}
