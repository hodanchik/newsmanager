package com.epam.lab.repository;

import com.epam.lab.model.Tag;
import com.epam.lab.specification.SearchSpecification;

import java.util.List;

public interface TagRepository {

    Tag findOne(long id);

    long save(Tag tag);

    boolean delete(long id);

    boolean update(Tag tag);

    List<Tag> findAll();

    List<Tag> findByName(String name);
    List<Tag> find(SearchSpecification specifications);



}
