package com.epam.lab.repository;

import com.epam.lab.exception.NotFoundByIdException;
import com.epam.lab.model.Author;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class AuthorRepositoryImpl implements AuthorRepository {
    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public Author findOne(long id) {
        Author authorById = entityManager.find(Author.class, id);
        if (authorById == null) {
            throw new NotFoundByIdException("Not Found author by id: " + id);
        }
        return authorById;
    }

    @Override
    public long save(Author author) {
        entityManager.persist(author);
        return author.getId();
    }

    @Override
    public boolean delete(long id) {
        entityManager.remove(findOne(id));
        return true;
    }

    @Override
    public boolean update(Author author) {
        entityManager.merge(author);
        return true;
    }

    @Override
    public List<Author> findAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Author> criteriaQuery = criteriaBuilder.createQuery(Author.class);
        Root<Author> authorRoot = criteriaQuery.from(Author.class);
        criteriaQuery.select(authorRoot);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }
}
