package com.epam.lab.exception;

public class NotFoundByIdException extends RepositoryException {

    public NotFoundByIdException(String message) {
        super(message);
    }
}
