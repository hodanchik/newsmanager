package com.epam.lab.specification;

import com.epam.lab.exception.RepositoryException;
import com.epam.lab.model.News;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class NewsSearchSpecification implements SearchSpecification<News> {

    public static final String REGEX_SPLIT = "_";
    public static final String EQUAL = ":";
    public static final String LIKE = "%";
    private SearchCriteria criteria;

    public NewsSearchSpecification(SearchCriteria criteria) {
        this.criteria = criteria;
    }


    @Override
    public Predicate toPredicate(Root<News> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        String[] keys = criteria.getKey().split(REGEX_SPLIT);
        if (keys.length == 1) {
            if (criteria.getOperation().equalsIgnoreCase(EQUAL)) {
                return builder.equal(
                        root.get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase(LIKE)
                    && (root.get(criteria.getKey()).getJavaType() == String.class)) {
                return builder.like(
                        root.get(criteria.getKey()), LIKE + criteria.getValue() + LIKE);
            }
        } else if (keys.length == 2) {
            String tableName = keys[0];
            String columnName = keys[1];
            if (criteria.getOperation().equalsIgnoreCase(EQUAL)) {
                return builder.equal(
                        root.join(tableName).get(columnName), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase(LIKE)) {
                return builder.like(
                        root.join(tableName).get(columnName), LIKE + criteria.getValue() + LIKE);
            }
        }
        throw new RepositoryException("Can't to create search specification");
    }
}