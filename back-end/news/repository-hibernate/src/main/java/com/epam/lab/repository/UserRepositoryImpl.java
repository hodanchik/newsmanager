package com.epam.lab.repository;

import com.epam.lab.model.Tag;
import com.epam.lab.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
@Repository
public class UserRepositoryImpl implements UserRepository {
    public static final String USER_LOGIN_FIELD = "login";
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public User findOne(long id) {
        return null;
    }

    @Override
    public long save(User user) {
        return 0;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public boolean update(User user) {
        return false;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User login(User user) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User>userRoot = criteriaQuery.from(User.class);
        criteriaQuery.select(userRoot);
        return entityManager.createQuery(criteriaQuery)
                .setParameter("login", user.getLogin())
                .setParameter("password", user.getPassword()).getSingleResult();
    }

    @Override
    public User findByUserName(String name) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User>userRoot = criteriaQuery.from(User.class);
        criteriaQuery.select(userRoot).where(criteriaBuilder.equal(userRoot.get(USER_LOGIN_FIELD), name));
        return entityManager.createQuery(criteriaQuery)
                .getSingleResult();
    }

}
