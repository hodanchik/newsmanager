package com.epam.lab.repository;

import com.epam.lab.model.Author;

import java.util.List;

public interface AuthorRepository {

    Author findOne(long id) ;

    long save(Author author) ;

    boolean delete(long id);

    boolean update(Author author) ;

    List<Author> findAll();
}
