package com.epam.lab.specification;

import com.epam.lab.exception.RepositoryException;
import com.epam.lab.model.News;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;


public class NewsSortSpecification implements SortSpecification<News> {
    public static final String REGEX_SPLIT = "_";
    public static final String CREATE_SPEC_EXCEPTION = "Can't to create sort specification";
    private SortCriteria sortCriteria;

    public NewsSortSpecification(SortCriteria sortCriteria) {
        this.sortCriteria = sortCriteria;
    }

    @Override
    public Order toOrder(Root<News> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        String[] keys = sortCriteria.getField().split(REGEX_SPLIT);
        if (keys.length == 1) {
            return getOrder(root, criteriaBuilder);
        } else if (keys.length == 2){
            String[] fieldKeys = sortCriteria.getField().split(REGEX_SPLIT);
            return getOrder(root, criteriaBuilder, fieldKeys);
        }
        throw new RepositoryException(CREATE_SPEC_EXCEPTION);
    }

    private Order getOrder(Root<News> root, CriteriaBuilder criteriaBuilder, String[] fieldKeys) {
        return sortCriteria.isDescOrder() ?
                criteriaBuilder.desc(root.join(fieldKeys[0]).get(fieldKeys[1])) :
                criteriaBuilder.asc(root.join(fieldKeys[0]).get(fieldKeys[1]));
    }

    private Order getOrder(Root<News> root, CriteriaBuilder criteriaBuilder) {
        return sortCriteria.isDescOrder() ?
                criteriaBuilder.desc(root.get(sortCriteria.getField())) :
                criteriaBuilder.asc(root.get(sortCriteria.getField()));
    }
}