package com.epam.lab.repository;

import com.epam.lab.exception.NotFoundByIdException;
import com.epam.lab.model.News;
import com.epam.lab.specification.SearchSpecification;

import com.epam.lab.specification.SortSpecification;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class NewsRepositoryImpl implements NewsRepository {
    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public News findOne(long id) {
        News newsById = entityManager.find(News.class, id);
        if(newsById == null){
            throw new NotFoundByIdException("Not Found News by id: " + id );
        }
        return newsById;
    }

    @Override
    public long save(News news) {
        entityManager.persist(news);
        return news.getId();
    }

    @Override
    public boolean delete(long id) {
        entityManager.remove(findOne(id));
        return true;
    }

    @Override
    public boolean update(News news) {
        findOne(news.getId());
        entityManager.merge(news);
        return true;
    }

    @Override
    public List<News> findAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);
        Root<News> newsRoot = criteriaQuery.from(News.class);
        criteriaQuery.select(newsRoot);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<News> findNews(SearchSpecification specifications, List<SortSpecification<News>> sortSpecifications) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);
        Root<News> newsRoot = criteriaQuery.from(News.class);
        criteriaQuery.where(specifications.toPredicate(newsRoot, criteriaQuery, criteriaBuilder));
        if (!sortSpecifications.isEmpty()) {
            criteriaQuery.orderBy(sortSpecifications
                    .stream()
                    .map(ss -> ss.toOrder(newsRoot, criteriaQuery, criteriaBuilder))
                    .collect(Collectors.toList()));
        }
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<News> findAllWithSort(List<SortSpecification<News>> sortSpecifications) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);
        Root<News> newsRoot = criteriaQuery.from(News.class);
        criteriaQuery.select(newsRoot);
        criteriaQuery.orderBy(sortSpecifications
                .stream()
                .map(ss -> ss.toOrder(newsRoot, criteriaQuery, criteriaBuilder))
                .collect(Collectors.toList()));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }


}

