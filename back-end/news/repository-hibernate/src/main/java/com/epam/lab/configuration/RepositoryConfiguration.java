package com.epam.lab.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

@Configuration
@ComponentScan("com.epam.lab.repository")
public class RepositoryConfiguration {

    public static final String HIBERNATE_DIALECT = "hibernate.dialect";
    public static final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    public static final String HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
    public static final String TRUE = "true";
    public static final String POSTGRE_SQLDIALECT = "org.hibernate.dialect.PostgreSQLDialect";
    private static final String CREATION_TABLES_SQL_SCRIPT_NAME = "db/schema.sql";
    private static final String FILL_TABLES_SQL_SCRIPT_NAME = "db/data.sql";

    @Bean
    public HikariConfig hikariConfig() throws IOException {
        Properties properties = new Properties();
        properties.load(getClass().getResourceAsStream("/datasource.properties"));
        return new HikariConfig(properties);
    }

    @Bean(destroyMethod = "close")
    @Profile("prod")
    public DataSource hikariDataSource(HikariConfig hikariConfig) {
        return new HikariDataSource(hikariConfig);
    }

    @Bean
    @Profile("dev")
    public DataSource embadedDataSource() {
        return new EmbeddedDatabaseBuilder()
                .addScript(CREATION_TABLES_SQL_SCRIPT_NAME)
                .addScript(FILL_TABLES_SQL_SCRIPT_NAME)
                .setType(EmbeddedDatabaseType.H2)
                .build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setPackagesToScan("com.epam.lab.model");
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setJpaDialect(new HibernateJpaDialect());
        factoryBean.setJpaProperties(additionalProperties());
        return factoryBean;
    }

    private Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty(HIBERNATE_DIALECT, POSTGRE_SQLDIALECT);
        properties.setProperty(HIBERNATE_SHOW_SQL, TRUE);
        properties.setProperty(HIBERNATE_FORMAT_SQL, TRUE);
        return properties;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager(emf);
        return transactionManager;
    }
}


