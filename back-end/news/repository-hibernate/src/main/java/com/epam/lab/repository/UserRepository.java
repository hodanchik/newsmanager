package com.epam.lab.repository;
import com.epam.lab.model.User;

import java.util.List;

public interface UserRepository {
    User findOne(long id);

    long save(User user);

    boolean delete(long id);

    boolean update(User user);

    List<User> findAll();

    User login(User user);

    User findByUserName(String name);
}
