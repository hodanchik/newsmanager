package com.epam.lab.repository;
import com.epam.lab.exception.NotFoundByIdException;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.specification.SearchSpecification;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.List;
import java.util.Set;

@Repository
public class TagRepositoryImpl implements TagRepository {
    public static final String TAG_NAME_FIELD = "name";
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Tag findOne(long id) {
        Tag tagById = entityManager.find(Tag.class, id);
        if(tagById == null){
            throw new NotFoundByIdException("Not Found Tag by id: " + id );
        }
        return entityManager.find(Tag.class, id);
    }

    @Override
    public long save(Tag tag) {
        entityManager.persist(tag);
        return tag.getId();
    }

//    @Override
//    public boolean delete(long id) {
//        entityManager.remove(findOne(id));
//        return true;
//    }
    @Override
    public boolean delete(long id) {
        Tag tag = findOne(id);
        Set<News> newsWithThisTag = tag.getNews();
        for (News news : newsWithThisTag) {
            news.getTags().remove(tag);
        }
        entityManager.remove(tag);
        return  true;
    }

    @Override
    public boolean update(Tag tag) {
        findOne(tag.getId());
        entityManager.merge(tag);
        return true;
    }

    @Override
    public List<Tag> findAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> criteriaQuery = criteriaBuilder.createQuery(Tag.class);
        Root<Tag> tagRoot = criteriaQuery.from(Tag.class);
        criteriaQuery.select(tagRoot);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<Tag> findByName(String name) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> criteriaQuery = criteriaBuilder.createQuery(Tag.class);
        Root<Tag> tagRoot = criteriaQuery.from(Tag.class);
        criteriaQuery.select(tagRoot).where(criteriaBuilder.equal(tagRoot.get(TAG_NAME_FIELD), name));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<Tag> find(SearchSpecification specifications) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> criteriaQuery = criteriaBuilder.createQuery(Tag.class);
        Root<Tag> newsRoot = criteriaQuery.from(Tag.class);
        criteriaQuery.where(specifications.toPredicate(newsRoot, criteriaQuery, criteriaBuilder));
        return entityManager.createQuery(criteriaQuery).getResultList();

    }

}
