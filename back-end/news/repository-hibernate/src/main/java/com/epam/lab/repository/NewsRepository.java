package com.epam.lab.repository;

import com.epam.lab.model.News;
import com.epam.lab.specification.SearchSpecification;
import com.epam.lab.specification.SortSpecification;

import java.util.List;

public interface NewsRepository {
    News findOne(long id);

    long save(News news);

    boolean delete(long id);

    boolean update(News news);

    List<News> findAll();

    List<News> findNews(SearchSpecification specifications, List<SortSpecification<News>> sortSpecifications);

    List<News> findAllWithSort(List<SortSpecification<News>> sortSpecifications);
}
