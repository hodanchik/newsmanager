package com.epam.lab.specification;

public class SortCriteria {
    public static final String DESC_ORDER = "desc";
    private String field;
    private String order;

    public SortCriteria(String field, String order) {
        this.field = field;
        this.order = order;
    }

    public boolean isDescOrder() {
        return DESC_ORDER.equalsIgnoreCase(order);
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

}

