package com.epam.lab.service;

import com.epam.lab.dto.AuthorDTO;
import com.epam.lab.dto.NewsDTO;
import com.epam.lab.dto.TagDTO;
import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.AuthorRepository;
import com.epam.lab.repository.NewsRepository;
import com.epam.lab.repository.TagRepository;
import com.epam.lab.specification.*;


import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;
@Transactional
@Service
public class NewsServiceImpl implements NewsService {

    private final static Logger LOG = LoggerFactory.getLogger(NewsServiceImpl.class);
    private NewsRepository newsRepository;
    private ModelMapper modelMapper;
    private AuthorRepository authorRepository;
    private TagRepository tagRepository;

    @Autowired
    public NewsServiceImpl(NewsRepository newsRepository, ModelMapper modelMapper, AuthorRepository authorRepository,
                           TagRepository tagRepository) {
        LOG.debug("NewsServiceImpl constructor");
        this.newsRepository = newsRepository;
        this.modelMapper = modelMapper;
        this.authorRepository = authorRepository;
        this.tagRepository = tagRepository;

    }
    @Override
    public NewsDTO findOne(Long newsId) {
        return convertNewsToDto(newsRepository.findOne(newsId));
    }

    @Override
    public NewsDTO save(NewsDTO newsDTO) {
        prepareNewsToSave(newsDTO);
        setDateWithCreate(newsDTO);
        long newsId = newsRepository.save(convertNewsToEntity(newsDTO));
        newsDTO.setId(newsId);
        return newsDTO;
    }

    private void prepareNewsToSave(NewsDTO newsDTO) {
        checkAndSaveAuthorUnique(newsDTO);
        checkAndSaveTagUnique(newsDTO);
    }

    private void setDateWithCreate(NewsDTO newsDTO) {
        newsDTO.setCreationDate(LocalDate.now());
        newsDTO.setModificationDate(LocalDate.now());
    }

    private void setDateWithUpdate(NewsDTO newsDTO) {
        newsDTO.setModificationDate(LocalDate.now());
    }

    private void checkAndSaveAuthorUnique(NewsDTO newsDTO) {
        if (newsDTO.getAuthorDTO().getId() == 0) {
            long newAuthorId = authorRepository.save(convertAuthorToEntity(newsDTO.getAuthorDTO()));
            newsDTO.getAuthorDTO().setId(newAuthorId);
        }
    }

    private void checkAndSaveTagUnique(NewsDTO newsDTO) {
        Set<TagDTO> tagsSet = newsDTO.getTagsSet();
        if (tagsSet != null) {
            for (TagDTO tagDTO : tagsSet) {
                addTagsId(tagDTO);
            }
        }
    }

    private void addTagsId(TagDTO tagDTO) {
        Tag tag = convertTagToEntity(tagDTO);
        long tagSaveId;
        if(tag.getId() != 0){
            tagSaveId = tag.getId();
        }
        else {
            List<Tag> newsTagsList = tagRepository.findByName(tag.getName());

            if (newsTagsList.isEmpty()) {
                tagSaveId = tagRepository.save(tag);
            } else {
                tagSaveId = newsTagsList.get(0).getId();
            }
        }
            tagDTO.setId(tagSaveId);

    }


    @Override
    public void delete(Long newsId) {
        newsRepository.delete(newsId);
    }


    @Override
    public NewsDTO update(NewsDTO newsDTO) {
        prepareNewsToSave(newsDTO);
        setDateWithUpdate(newsDTO);
        newsRepository.update(convertNewsToEntity(newsDTO));
        return findOne(newsDTO.getId());
    }

    @Override
    public List<NewsDTO> findAll() {

        return getNewsDtoList(newsRepository.findAll());
    }


    @Override
    public List<NewsDTO> find(List<SearchCriteria> searchCriteriaList, List<SortCriteria> sortCriteriaList) {
         NewsSpecificationBuilder builder = new NewsSpecificationBuilder();
        if (searchCriteriaList.isEmpty() && sortCriteriaList.isEmpty()) {
            return findAll();
        } else if (searchCriteriaList.isEmpty() && !sortCriteriaList.isEmpty()) {
            return findAllWithSort(sortCriteriaList);
        } else {
            for (SearchCriteria searchCriteria : searchCriteriaList) {
                builder.with(searchCriteria);
            }
            SearchSpecification<News> spec = builder.build();
            List<SortSpecification<News>> sortSpecifications = getSortSpecificationsBySortCriteria(sortCriteriaList);
            return getNewsDtoList(newsRepository.findNews(spec, sortSpecifications));
        }
    }

    @Override
    public List<NewsDTO> findAllWithSort(List<SortCriteria> sortCriteriaList) {
        List<SortSpecification<News>> sortSpecifications = getSortSpecificationsBySortCriteria(sortCriteriaList);
        return getNewsDtoList(newsRepository.findAllWithSort(sortSpecifications));
    }

    private List<SortSpecification<News>> getSortSpecificationsBySortCriteria(List<SortCriteria> sortCriteriaList) {
        List<SortSpecification<News>> sortSpecifications = new ArrayList<>();
        if (!sortCriteriaList.isEmpty()) {
            for (SortCriteria sc : sortCriteriaList) {
                sortSpecifications.add(new NewsSortSpecification(sc));
            }
        }
        return sortSpecifications;
    }


    private List<NewsDTO> getNewsDtoList(List<News> newsList) {
        List<NewsDTO> newsDTOList = new ArrayList<>();
        for (News newsFromList : newsList) {
            newsDTOList.add(convertNewsToDto(newsFromList));
        }
        return newsDTOList;
    }


    private NewsDTO convertNewsToDto(News news) {
        NewsDTO newsDto = modelMapper.map(news, NewsDTO.class);
        newsDto.setAuthorDTO(convertAuthorToDto(news.getAuthor()));
        newsDto.setTagsSet(convertTagsSetToDto(news.getTags()));
        return newsDto;
    }

    private News convertNewsToEntity(NewsDTO newsDto) {
        News news = modelMapper.map(newsDto, News.class);
        news.setAuthor(convertAuthorToEntity(newsDto.getAuthorDTO()));
        news.setTags(convertTagsSetToEntity(newsDto.getTagsSet()));
        return news;
    }

    private AuthorDTO convertAuthorToDto(Author author) {
        return modelMapper.map(author, AuthorDTO.class);
    }

    private Author convertAuthorToEntity(AuthorDTO authorDTO) {
        return modelMapper.map(authorDTO, Author.class);
    }

    private Tag convertTagToEntity(TagDTO tagDTO) {
        return modelMapper.map(tagDTO, Tag.class);
    }

    private Set<TagDTO> convertTagsSetToDto(Set<Tag> tags) {
        Set<TagDTO> tagsDtoList = new HashSet<>();
        for (Tag tag : tags) {
            tagsDtoList.add(modelMapper.map(tag, TagDTO.class));
        }
        return tagsDtoList;
    }

    private Set<Tag> convertTagsSetToEntity(Set<TagDTO> tags) {
        Set<Tag> tagsList = new HashSet<>();
        for (TagDTO tagDTO : tags) {
            tagsList.add(modelMapper.map(tagDTO, Tag.class));
        }
        return tagsList;
    }
}
