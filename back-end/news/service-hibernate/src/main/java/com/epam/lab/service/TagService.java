package com.epam.lab.service;

import com.epam.lab.dto.TagDTO;
import com.epam.lab.specification.SearchCriteria;

import java.util.List;

public interface TagService extends BaseService<Long, TagDTO> {

    TagDTO findOne(Long id) ;

    TagDTO save(TagDTO tagDTO) ;

    void delete(Long id);

    TagDTO update(TagDTO tagDTO) ;

    List<TagDTO> findAll();
    List<TagDTO> find(List<SearchCriteria> searchCriteriaList);

}
