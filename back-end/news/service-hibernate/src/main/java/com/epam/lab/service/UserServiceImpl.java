package com.epam.lab.service;
import com.epam.lab.dto.UserDTO;
import com.epam.lab.model.User;
import com.epam.lab.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl implements UserService, UserDetailsService {
    private UserRepository userRepository;
    private ModelMapper modelMapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;

    }
    @Override
    public UserDTO findOne(Long id) {
        return null;
    }

    @Override
    public UserDTO findByUserName(String name) {
       return convertToDto(userRepository.findByUserName(name));
    }

    @Override
    public UserDTO save(UserDTO userDto) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public UserDTO update(UserDTO userDto) {
        return null;
    }

    @Override
    public List<UserDTO> findAll() {
        return null;
    }


    private UserDTO convertToDto(User user) {
        return modelMapper.map(user, UserDTO.class);
    }

    private User convertToEntity(UserDTO userDTO) {
        return modelMapper.map(userDTO, User.class);
    }

    @Override
    public UserDTO loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDTO UserDto = this.findByUserName(username);
        if(UserDto ==null){
            throw new UsernameNotFoundException(username);
        }
        return UserDto;
    }
}
