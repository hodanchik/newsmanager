package com.epam.lab.dto;

import com.epam.lab.model.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class UserDTO implements UserDetails {
    @PositiveOrZero
    private long id;
//    @NotBlank(message = "Name can't be empty")
//    @Size(min = 1, max = 30, message = "Name must be between 1 and 30 characters")
    private String name;
//    @NotBlank(message = "Surname can't be empty")
//    @Size(min = 1, max = 30, message = "Surname must be between 1 and 30 characters")
    private String surname;
    @NotBlank(message = "login can't be empty")
    @Size(min = 1, max = 30, message = "login must be between 1 and 30 characters")
    private String login;
    @NotBlank(message = "password can't be empty")
    @Size(min = 1, max = 30, message = "password must be between 1 and 30 characters")
    private String password;
    private List<RoleDTO> roles;

    public UserDTO() {
    }

    public UserDTO( String login,String password) {
        this.login = login;
        this.password = password;
    }

    public UserDTO(long id, String name, String surname, String login, String password) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<RoleDTO> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleDTO> roles) {
        this.roles = roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserDTO)) return false;
        UserDTO userDTO = (UserDTO) o;
        return getId() == userDTO.getId() &&
                Objects.equals(getName(), userDTO.getName()) &&
                Objects.equals(getSurname(), userDTO.getSurname()) &&
                Objects.equals(getLogin(), userDTO.getLogin()) &&
                Objects.equals(getPassword(), userDTO.getPassword()) &&
                Objects.equals(getRoles(), userDTO.getRoles());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getSurname(), getLogin(), getPassword(), getRoles());
    }
}
