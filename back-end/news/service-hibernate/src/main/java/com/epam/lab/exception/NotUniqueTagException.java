package com.epam.lab.exception;

public class NotUniqueTagException extends RepositoryException {

    public NotUniqueTagException(String message) {
        super(message);
    }
}
