package com.epam.lab.service;

import com.epam.lab.dto.NewsDTO;
import com.epam.lab.specification.SearchCriteria;
import com.epam.lab.specification.SortCriteria;

import java.util.List;

public interface NewsService extends BaseService<Long, NewsDTO> {

    NewsDTO findOne(Long id);

    NewsDTO save(NewsDTO newsDTO);

    void delete(Long id);

    NewsDTO update(NewsDTO newsDTO);

    List<NewsDTO> findAll();

    List<NewsDTO> find(List<SearchCriteria> searchCriteria, List<SortCriteria> sortList);

    List<NewsDTO> findAllWithSort( List<SortCriteria> sortList);

}
