package com.epam.lab.dto;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.Objects;

public class TagDTO {
    @PositiveOrZero
    private long id;
    @NotBlank(message = "Tag name can't be empty")
    @Size(min = 1, max = 30, message = "Tag name must be between 1 and 30 characters")
    private String name;

    public TagDTO() {
    }

    public TagDTO(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public TagDTO(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TagDTO)) return false;
        TagDTO tagDTO = (TagDTO) o;
        return getId() == tagDTO.getId() &&
                getName().equals(tagDTO.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }
}
