package com.epam.lab.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.Objects;

public class AuthorDTO {
    @PositiveOrZero
    private long id;
    @NotBlank(message = "Name can't be empty")
    @Size(min = 1, max = 30, message = "Name must be between 1 and 30 characters")
    private String name;
    @NotBlank(message = "Surname can't be empty")
    @Size(min = 1, max = 30, message = "Surname must be between 1 and 30 characters")
    private String surname;

    public AuthorDTO() {
    }

    public AuthorDTO(long id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

    public AuthorDTO(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuthorDTO)) return false;
        AuthorDTO authorDTO = (AuthorDTO) o;
        return getId() == authorDTO.getId() &&
                getName().equals(authorDTO.getName()) &&
                getSurname().equals(authorDTO.getSurname());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getSurname());
    }
}
