package com.epam.lab.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

public class NewsDTO {
    @PositiveOrZero
    private long id;
    @NotBlank(message = "Tittle can't be empty")
    @Size(min = 1, max = 30, message = "Title must be between 1 and 30 characters")
    private String title;
    @NotBlank(message = "Short text can't be empty")
    @Size(min = 1, max = 30, message = "Short text must be between 1 and 100 characters")
    private String shortText;
    @NotBlank(message = "Full text can't be empty")
    @Size(min = 1, max = 30, message = "Full text must be between 1 and 100 characters")
    private String fullText;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate creationDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate modificationDate;
    private AuthorDTO authorDTO;
    private Set<TagDTO> tagsSet;

    public NewsDTO() {
    }

    public NewsDTO(long id, String title, String shortText, String fullText, LocalDate creationDate,
                   LocalDate modificationDate, AuthorDTO authorDTO, Set<TagDTO> tagsList) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
        this.authorDTO = authorDTO;
        this.tagsSet = tagsList;

    }

    public NewsDTO(long id, String title, String shortText, String fullText, LocalDate creationDate,
                   AuthorDTO authorDTO, Set<TagDTO> tagsList) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.authorDTO = authorDTO;
        this.tagsSet = tagsList;
    }

    public NewsDTO(String title, String shortText, String fullText, AuthorDTO authorDTO, Set<TagDTO> tagsList) {
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.authorDTO = authorDTO;
        this.tagsSet = tagsList;
        this.creationDate = LocalDate.now();
        this.modificationDate = creationDate;
    }


    public NewsDTO(long id, String title, String shortText, String fullText, AuthorDTO newAuthorDTO, Set<TagDTO> tagDTOList) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.authorDTO = newAuthorDTO;
        this.tagsSet = tagDTOList;
        this.creationDate = LocalDate.now();
        this.modificationDate = creationDate;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
    }

    public AuthorDTO getAuthorDTO() {
        return authorDTO;
    }

    public void setAuthorDTO(@NotNull(message = "News must has Author") AuthorDTO authorDTO) {
        this.authorDTO = authorDTO;
    }

    public Set<TagDTO> getTagsSet() {
        return tagsSet;
    }

    public void setTagsSet(Set<TagDTO> tagsSet) {
        this.tagsSet = tagsSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NewsDTO)) return false;
        NewsDTO newsDTO = (NewsDTO) o;
        return getId() == newsDTO.getId() &&
                getTitle().equals(newsDTO.getTitle()) &&
                getShortText().equals(newsDTO.getShortText()) &&
                getFullText().equals(newsDTO.getFullText()) &&
                getCreationDate().equals(newsDTO.getCreationDate()) &&
                getModificationDate().equals(newsDTO.getModificationDate()) &&
                getAuthorDTO().equals(newsDTO.getAuthorDTO()) &&
                getTagsSet().equals(newsDTO.getTagsSet());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getShortText(), getFullText(), getCreationDate(), getModificationDate(), getAuthorDTO(), getTagsSet());
    }
}
