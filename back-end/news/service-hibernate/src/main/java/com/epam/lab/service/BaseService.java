package com.epam.lab.service;
import java.util.List;

public interface BaseService<K, E> {
    E findOne(K id) ;

    E save(E entity) ;

    void delete(K id);

    E update(E entity);

    List<E> findAll();

}
