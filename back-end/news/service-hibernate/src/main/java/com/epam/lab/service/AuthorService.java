package com.epam.lab.service;


import com.epam.lab.dto.AuthorDTO;

import java.util.List;

public interface AuthorService  extends BaseService<Long, AuthorDTO>{

    AuthorDTO findOne(Long id) ;

    AuthorDTO save(AuthorDTO authorDto) ;

    void delete(Long id) ;

    AuthorDTO update(AuthorDTO authorDto) ;

    List<AuthorDTO> findAll();

}
