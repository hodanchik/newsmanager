package com.epam.lab.service;

import com.epam.lab.dto.AuthorDTO;
import com.epam.lab.model.Author;
import com.epam.lab.repository.AuthorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
@Transactional
@Service
public class AuthorServiceImpl implements AuthorService {
    private AuthorRepository authorRepository;
    private ModelMapper modelMapper;

    @Autowired
    public AuthorServiceImpl(AuthorRepository authorRepository, ModelMapper modelMapper) {
        this.authorRepository = authorRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public AuthorDTO findOne(Long id) {
        Author author = authorRepository.findOne(id);
        return convertToDto(author);
    }

    @Override
    public AuthorDTO save(AuthorDTO authorDto) {
        Author author = convertToEntity(authorDto);
        long saveAuthor = authorRepository.save(author);
        return convertToDto(authorRepository.findOne(saveAuthor));
    }

    @Override
    public void delete(Long id) {
        authorRepository.delete(id);
    }

    @Override
    public AuthorDTO update(AuthorDTO authorDto) {

        authorRepository.update(convertToEntity(authorDto));
        Author authorAfterUpdate = authorRepository.findOne(authorDto.getId());
        return convertToDto(authorAfterUpdate);
    }

    @Override
    public List<AuthorDTO> findAll() {
        List<Author> authorsList = authorRepository.findAll();
        List<AuthorDTO> authorsDTOList = new ArrayList<>();
        for (Author author : authorsList) {
            authorsDTOList.add(convertToDto(author));
        }
        return authorsDTOList;
    }

    private AuthorDTO convertToDto(Author author) {
        return modelMapper.map(author, AuthorDTO.class);
    }

    private Author convertToEntity(AuthorDTO authorDto) {
        return modelMapper.map(authorDto, Author.class);
    }
}

