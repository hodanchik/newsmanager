package com.epam.lab.service;

import com.epam.lab.dto.TagDTO;
import com.epam.lab.exception.NotUniqueTagException;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.TagRepository;
import com.epam.lab.specification.SearchCriteria;
import com.epam.lab.specification.SearchSpecification;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Transactional
@Service
public class TagServiceImpl implements TagService {
    private TagRepository tagRepository;
    private ModelMapper modelMapper;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository, ModelMapper modelMapper) {
        this.tagRepository = tagRepository;
        this.modelMapper = modelMapper;

    }
    @Override
    public TagDTO findOne(Long id) {
        return convertToDto(tagRepository.findOne(id));
    }

    @Override
    public TagDTO save(TagDTO tagDTO) {
        long tagSaveId = checkAndSaveTagUnique(tagDTO);
        return convertToDto(tagRepository.findOne(tagSaveId));
    }

    private long checkAndSaveTagUnique(TagDTO tagDTO) {
        Tag tag = convertToEntity(tagDTO);
        List<Tag> newsTagsList = tagRepository.findByName(tag.getName());
        long tagSaveId;
        if (newsTagsList.isEmpty()) {
            tagSaveId = tagRepository.save(tag);
        } else {
            tagSaveId = newsTagsList.get(0).getId();
        }
        return tagSaveId;
    }

    @Override
    public void delete(Long id) {
        tagRepository.delete(id);
    }

    @Override
    public TagDTO update(TagDTO tagDTO) {
        List<Tag> newsTagsList = tagRepository.findByName(tagDTO.getName());
        if (newsTagsList.isEmpty()) {
            tagRepository.update(convertToEntity(tagDTO));
        } else {
         throw new NotUniqueTagException("Updated tag is not unique");
        }
        Tag tagAfterUpdate = tagRepository.findOne(tagDTO.getId());
        return convertToDto(tagAfterUpdate);
    }

    @Override
    public List<TagDTO> findAll() {
        return convertTagListToDto(tagRepository.findAll());
    }

    @Override
    public List<TagDTO> find(List<SearchCriteria> searchCriteriaList) {
        NewsSpecificationBuilder builder = new NewsSpecificationBuilder();
        if (searchCriteriaList.isEmpty()) {
            return findAll();
        }
        for (SearchCriteria searchCriteria : searchCriteriaList) {
            builder.with(searchCriteria);
        }
        SearchSpecification<News> spec = builder.build();
        return convertTagListToDto(tagRepository.find(spec));
    }

    private TagDTO convertToDto(Tag tag) {
        return modelMapper.map(tag, TagDTO.class);
    }

    private Tag convertToEntity(TagDTO tagDTO) {
        return modelMapper.map(tagDTO, Tag.class);
    }

    private List<TagDTO> convertTagListToDto(List<Tag> tagList) {
        List<TagDTO> tagDTOList = new ArrayList<>();
        for (Tag tag : tagList) {
            tagDTOList.add(convertToDto(tag));
        }
        return tagDTOList;
    }
}
