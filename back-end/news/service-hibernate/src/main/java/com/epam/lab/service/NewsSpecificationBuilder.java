package com.epam.lab.service;

import com.epam.lab.model.News;
import com.epam.lab.specification.NewsSearchSpecification;
import com.epam.lab.specification.SearchCriteria;
import com.epam.lab.specification.SearchSpecification;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
@Component
public class NewsSpecificationBuilder {

    private final List<SearchCriteria> params;

    public NewsSpecificationBuilder() {
        params = new ArrayList<>();
    }

    public NewsSpecificationBuilder with(SearchCriteria criteria) {
        params.add(criteria);
        return this;
    }

    public SearchSpecification<News> build() {
        if (params.isEmpty()) {
            return null;
        }

        List<SearchSpecification> specs = params.stream()
                .map(NewsSearchSpecification::new)
                .collect(Collectors.toList());

        SearchSpecification result = specs.get(0);

        for (int i = 1; i < params.size(); i++) {
            result = result.and(specs.get(i));
        }
        return result;
    }
}

