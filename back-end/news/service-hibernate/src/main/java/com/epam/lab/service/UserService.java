package com.epam.lab.service;

import com.epam.lab.dto.UserDTO;

import java.util.List;

public interface UserService extends BaseService<Long, UserDTO> {

    UserDTO findOne(Long id);

    UserDTO findByUserName(String name);

    UserDTO save(UserDTO userDto);

    void delete(Long id);

    UserDTO update(UserDTO userDto);

    List<UserDTO> findAll();

}
