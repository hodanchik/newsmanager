package com.epam.lab.service;

import com.epam.lab.configuration.RepositoryConfiguration;
import com.epam.lab.configuration.ServiceConfiguration;
import com.epam.lab.dto.AuthorDTO;
import com.epam.lab.dto.NewsDTO;
import com.epam.lab.dto.TagDTO;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.AuthorRepository;
import com.epam.lab.repository.NewsRepository;
import com.epam.lab.repository.TagRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {RepositoryConfiguration.class,
//        ServiceConfiguration.class, TestConfiguration.class})
//@ActiveProfiles("dev")
//@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)

public class NewsTransactionTest {
//    @Autowired
//    private  NewsService newsService;
//    @Autowired
//    private NewsRepository newsRepository;
//    @Autowired
//    private AuthorRepository authorRepository;
//    @Autowired
//    private TagRepository tagRepository;
//
//    private static NewsDTO newsDTOForSaveWithNewAuthorAndTagsException;
//    private static NewsDTO newsDTOForSaveWithNewAuthorAndTags;
//    private static AuthorDTO authorDTOForSaveNew;
//    private static Set<TagDTO> tagDTOForSaveExceptionList;
//    private static Set<TagDTO> tagDTOForSaveNew;
//    private static  List<Tag> tagListWithTag;
//
//    @Before
//    public  void setUp() {
//        authorDTOForSaveNew = new AuthorDTO("NewAuthor", "SurnameNewAuthor");
//        tagDTOForSaveExceptionList = new HashSet<>();
//        tagDTOForSaveExceptionList.add(new TagDTO("TAG_WITH_EXCEPTION"));
//        newsDTOForSaveWithNewAuthorAndTagsException = new NewsDTO("Title-save", "Short text in test01",
//                "Some text of news in tes", authorDTOForSaveNew, tagDTOForSaveExceptionList);
//
//        tagDTOForSaveNew = new HashSet<>();
//        tagDTOForSaveNew.add(new TagDTO("TAG_WITHOUT_EXCEPTION"));
//        newsDTOForSaveWithNewAuthorAndTags = new NewsDTO("Title-save", "Short text in test01",
//                "Some text of news in tes", authorDTOForSaveNew, tagDTOForSaveNew);
//    }
//
//
//    @Test
//    public void saveWithExceptionAndRollBack() {
//        Mockito.when(tagRepository.findByName("TAG_WITH_EXCEPTION")).thenThrow(RuntimeException.class);
//        int sizeAuthorList = authorRepository.findAll().size();
//        try {
//            newsService.save(newsDTOForSaveWithNewAuthorAndTagsException);
//        }
//        catch (RuntimeException e){
//
//        }
//        int sizeAuthorListAfterRollBack = authorRepository.findAll().size();
//        assertEquals(sizeAuthorList, sizeAuthorListAfterRollBack);
//    }

//    @Test
//    public void saveWithoutException() {
//        int sizeAuthorList = authorRepository.findAll().size();
//        newsService.save(newsDTOForSaveWithNewAuthorAndTags);
//        int sizeAuthorListAfterSaveNews = authorRepository.findAll().size();
//        assertEquals(20, sizeAuthorList);
//        assertEquals(21, sizeAuthorListAfterSaveNews);
//    }
}
