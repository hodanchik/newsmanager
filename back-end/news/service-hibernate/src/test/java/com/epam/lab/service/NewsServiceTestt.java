package com.epam.lab.service;

import com.epam.lab.dto.AuthorDTO;
import com.epam.lab.dto.NewsDTO;
import com.epam.lab.dto.TagDTO;
import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.AuthorRepository;
import com.epam.lab.repository.NewsRepository;
import com.epam.lab.repository.TagRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NewsServiceTestt {
//    private NewsService newsService;
//    private TagRepository mockTagRepository;
//    private AuthorRepository mockAuthorRepository;
//    private NewsRepository mockNewsRepository;
//
//    private News newsForSelect;
//    private News newsForSelectAfterSave;
//    private NewsDTO newsForSelectDTO;
//    private Author authorForSelect;
//    private Set<Tag> setTagForSelect;
//    private AuthorDTO authorForSelectDTO;
//    private Set<TagDTO> listTagForSelectDTO;
//    private NewsDTO newsDTOForSuccessSaveWithOldAuthorAndTags;
//    private NewsDTO newsDTOForSuccessUpdateWithOldAuthorAndTags;
//    private NewsDTO newsDTOAfterSuccessUpdateWithOldAuthorAndTags;
//    private News newsAfterUpdateWithOldAuthorAndTags;
//    private News newsForSuccessSave;
//    private NewsDTO newsDTOForSuccessSaveWithOldAuthorAndTagsSelect;
//    private List<Tag> tagsListBeNameOne;
//    private List<Tag> tagsListBeNameTwo;
//
//
//    @Before
//    public void setUp() {
//
//
//        setTagForSelect = new HashSet<>();
//        setTagForSelect.add(new Tag(1, "tag-test01"));
//        setTagForSelect.add(new Tag(2, "tag-test02"));
//        authorForSelect = new Author(1, "Name-test01", "Surname-test01");
//        newsForSelectAfterSave = new News(2, "Title-save", "Short text in test01",
//                "Some text of news in test01", LocalDate.now(), LocalDate.now(), setTagForSelect, authorForSelect);
//        newsForSelect = new News(1, "Title-test01", "Short text in test01", "Some text of news in test01",
//                LocalDate.of(2020, 01, 28), LocalDate.of(2020, 01, 28),
//                setTagForSelect, authorForSelect);
//        newsForSuccessSave = new News("Title-save", "Short text in test01",
//                "Some text of news in test01",
//                LocalDate.now(), LocalDate.now(),
//                setTagForSelect, authorForSelect);
//
//        listTagForSelectDTO = new HashSet<>();
//        listTagForSelectDTO.add(new TagDTO(1, "tag-test01"));
//        listTagForSelectDTO.add(new TagDTO(2, "tag-test02"));
//        authorForSelectDTO = new AuthorDTO(1, "Name-test01", "Surname-test01");
//
//        newsForSelectDTO = new NewsDTO(1, "Title-test01", "Short text in test01", "Some text of news in test01",
//                LocalDate.of(2020, 01, 28), LocalDate.of(2020, 01, 28),
//                authorForSelectDTO, listTagForSelectDTO);
//
//        newsDTOForSuccessSaveWithOldAuthorAndTags = new NewsDTO("Title-save", "Short text in test01",
//                "Some text of news in test01", authorForSelectDTO, listTagForSelectDTO);
//        newsDTOForSuccessSaveWithOldAuthorAndTagsSelect = new NewsDTO(2, "Title-save", "Short text in test01",
//                "Some text of news in test01", authorForSelectDTO, listTagForSelectDTO);
//
//
//        newsDTOForSuccessUpdateWithOldAuthorAndTags = new NewsDTO(3, "Title-update", "Short update",
//                "Some text update", LocalDate.of(2010, 01, 28), authorForSelectDTO, listTagForSelectDTO);
//
//        newsAfterUpdateWithOldAuthorAndTags = new News(3, "Title-update", "Short update",
//                "Some text update", LocalDate.of(2010, 01, 28), LocalDate.now(), setTagForSelect, authorForSelect);
//        newsDTOAfterSuccessUpdateWithOldAuthorAndTags = new NewsDTO(3, "Title-update", "Short update",
//                "Some text update", LocalDate.of(2010, 01, 28), LocalDate.now(), authorForSelectDTO, listTagForSelectDTO);
//
//        tagsListBeNameOne = new ArrayList<>();
//        tagsListBeNameOne.add(new Tag(1, "tag-test01"));
//        tagsListBeNameTwo = new ArrayList<>();
//        tagsListBeNameTwo.add(new Tag(2, "tag-test02"));
//
//
//        mockNewsRepository = mockNewsRepository();
//        ModelMapper modelMapper = new ModelMapper();
//        mockTagRepository = mockTagRepository();
//        newsService = new NewsServiceImpl(mockNewsRepository, modelMapper, mockAuthorRepository,
//                mockTagRepository);
//    }
//
//
//    private NewsRepository mockNewsRepository() {
//        NewsRepository mockNewsRepository = Mockito.mock(NewsRepository.class);
//        Mockito.when(mockNewsRepository.findOne(1L)).thenReturn(newsForSelect);
//        Mockito.when(mockNewsRepository.findOne(2L)).thenReturn(newsForSelectAfterSave);
//        Mockito.when(mockNewsRepository.save(newsForSuccessSave)).thenReturn(2L);
//
//
//        Mockito.when(mockNewsRepository.delete(1L)).thenReturn(true);
//        Mockito.when(mockNewsRepository.findOne(3L)).thenReturn(newsAfterUpdateWithOldAuthorAndTags);
//        return mockNewsRepository;
//    }
//
//    private TagRepository mockTagRepository() {
//        TagRepository mockTagRepository = Mockito.mock(TagRepository.class);
//        Mockito.when(mockTagRepository.findByName("tag-test01")).thenReturn(tagsListBeNameOne);
//        Mockito.when(mockTagRepository.findByName("tag-test02")).thenReturn(tagsListBeNameTwo);
//        return mockTagRepository;
//    }
//
//    @Test
//    public void findOne() {
//        NewsDTO newsByIdDto = newsService.findOne(1L);
//        Assert.assertEquals(newsForSelectDTO, newsByIdDto);
//    }
//
//    @Test
//    public void save() {
//        NewsDTO newsDTOAfterSave = newsService.save(newsDTOForSuccessSaveWithOldAuthorAndTags);
//        Assert.assertEquals(newsDTOForSuccessSaveWithOldAuthorAndTagsSelect, newsDTOAfterSave);
//    }
//
//    @Test
//    public void delete() {
//        newsService.delete(1L);
//        Mockito.verify(mockNewsRepository, Mockito.times(1)).delete(1L);
//    }
//
//    @Test
//    public void update() {
//        NewsDTO updateNewsDTO = newsService.update(newsDTOForSuccessUpdateWithOldAuthorAndTags);
//        Assert.assertEquals(newsDTOAfterSuccessUpdateWithOldAuthorAndTags, updateNewsDTO);
//    }

}
