package com.epam.lab.service;

import com.epam.lab.dto.TagDTO;
import com.epam.lab.exception.NotUniqueTagException;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.TagRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
//@Transactional

public class TagServiceTest  {
//    private TagService tagService;
//    private TagRepository mockTagRepository;
//    private TagDTO tagDtoForSave;
//    private TagDTO tagDtoAfterSave;
//    private Tag tagForSave;
//    private List<Tag> tagNotUniqueList;
//    private Tag tagForUpdateWithException;
//    private TagDTO tagDtoForUpdateWithException;
//    private Tag tagAfterSave;
//    private TagDTO tagDtoBeforeUpdate;
//    private TagDTO tagDtoAfterUpdate;
//    private Tag tagBeforeUpdate;
//    private Tag tagAfterUpdate;
//    private List<TagDTO> allTagListDto;
//    private List<Tag> allTagList;
//
//    @Before
//    public void setUp() {
//        tagDtoForSave = new TagDTO("testTag");
//        tagDtoAfterSave = new TagDTO(1, "testTag");
//        tagForSave = new Tag();
//        tagForSave.setName("testTag");
//        tagAfterSave = new Tag(1, "testTag");
//        tagDtoBeforeUpdate = new TagDTO(2, "tagBeforeUpdate");
//        tagDtoAfterUpdate = new TagDTO(2, "tagAfterUpdate");
//        tagBeforeUpdate = new Tag(2, "tagBeforeUpdate");
//        tagAfterUpdate = new Tag(2, "tagAfterUpdate");
//        tagForUpdateWithException = new Tag(3,"notUniqueName");
//        tagDtoForUpdateWithException = new TagDTO(3,"notUniqueName");
//        tagNotUniqueList = new ArrayList<>();
//        tagNotUniqueList.add(tagForUpdateWithException);
//        allTagList = new ArrayList<>();
//        allTagListDto = new ArrayList<>();
//        allTagListDto.add(tagDtoAfterSave);
//        allTagListDto.add(tagDtoAfterUpdate);
//        allTagList.add(tagAfterSave);
//        allTagList.add(tagAfterUpdate);
//        mockTagRepository = mockTagRepository();
//        ModelMapper modelMapper = new ModelMapper();
//        tagService = new TagServiceImpl(mockTagRepository, modelMapper);
//    }
//
//
//    private TagRepository mockTagRepository() {
//        TagRepository mockTagRepository = Mockito.mock(TagRepository.class);
//        Mockito.when(mockTagRepository.save(tagForSave)).thenReturn(1L);
//               Mockito.when(mockTagRepository.delete(1L)).thenReturn(true);
//        Mockito.when(mockTagRepository.findOne(1L)).thenReturn(tagAfterSave);
//                Mockito.when(mockTagRepository.findOne(2L)).thenReturn(tagAfterUpdate);
//        Mockito.when(mockTagRepository.findAll()).thenReturn(allTagList);
//        Mockito.when(mockTagRepository.update(tagBeforeUpdate)).thenReturn(true);
//        Mockito.when(mockTagRepository.findByName(tagForUpdateWithException.getName())).thenReturn(tagNotUniqueList);
//        return mockTagRepository;
//    }
//
//    @Test
//    public void testFindOne() {
//        TagDTO tagByIdDto = tagService.findOne(1L);
//        Mockito.verify(mockTagRepository, Mockito.times(1)).findOne(1L);
//        assertEquals(tagDtoAfterSave, tagByIdDto);
//    }
//
//    @Test
//    public void testSave() {
//        TagDTO savedTagDto = tagService.save(tagDtoForSave);
//        assertEquals(tagDtoAfterSave, savedTagDto);
//    }
//
//    @Test
//    public void testDelete(){
//        tagService.delete(1L);
//        Mockito.verify(mockTagRepository, Mockito.times(1)).delete(1L);
//    }
//
//    @Test
//    public void testUpdate()  {
//        TagDTO tagAfterUpdateDto = tagService.update(tagDtoBeforeUpdate);
//        assertEquals(tagDtoAfterUpdate, tagAfterUpdateDto);
//    }
//    @Test(expected = NotUniqueTagException.class)
//    public void testUpdateException()  {
//        tagService.update(tagDtoForUpdateWithException);
//    }
//
//    @Test
//    public void testFindAll() {
//        List<TagDTO> tagList = tagService.findAll();
//        Mockito.verify(mockTagRepository, Mockito.times(1)).findAll();
//        assertEquals(allTagListDto, tagList);
//    }
//

}