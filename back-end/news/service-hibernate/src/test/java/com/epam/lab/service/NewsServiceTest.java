package com.epam.lab.service;

import com.epam.lab.configuration.RepositoryConfiguration;
import com.epam.lab.configuration.ServiceConfiguration;
import com.epam.lab.dto.AuthorDTO;
import com.epam.lab.dto.NewsDTO;
import com.epam.lab.dto.TagDTO;
import com.epam.lab.model.Author;
import com.epam.lab.specification.SearchCriteria;
import com.epam.lab.specification.SortCriteria;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
//@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {RepositoryConfiguration.class,
//        ServiceConfiguration.class})
//@ActiveProfiles("dev")
public class NewsServiceTest {
//
//    @Autowired
//    private NewsService newsService;
//    private NewsSpecificationBuilder builder;
//    private NewsDTO newsDTOForSelect;
//    private AuthorDTO authorDTOForSelect;
//    private AuthorDTO authorDTOForSaveNew;
//    private Author authorForSaveNew;
//    private Author authorAfterSaveNew;
//    private TagDTO tagDTOForSelectOne;
//    private TagDTO tagDTOForSelectTwo;
//    private Set<TagDTO> tagsList;
//    private Set<TagDTO> tagsListNew;
//    private TagDTO tagDTONewForSave;
//    private NewsDTO newsDTOForSuccessSaveWithOldAuthorAndTags;
//    private NewsDTO newsDTOForSuccessSaveWithNewAuthorAndOldTags;
//    private NewsDTO newsDTOForSuccessSaveWithNewAuthorAndNewTags;
//    private NewsDTO newsDTOForUpdate;
//    private List<SearchCriteria> searchCriteriaList;
//    private List<SortCriteria> sortCriteriaList;
//
//    @Before
//    public void setUp() {
//        builder = new NewsSpecificationBuilder();
//
//        authorDTOForSelect = new AuthorDTO(1, "Name-test01", "Surname-test01");
//        authorDTOForSaveNew = new AuthorDTO("NewAuthor", "SurnameNewAuthor");
//        authorForSaveNew = new Author("NewAuthor", "SurnameNewAuthor");
//        authorAfterSaveNew = new Author(21, "NewAuthor", "SurnameNewAuthor");
//        tagDTOForSelectOne = new TagDTO(1, "tag-test01");
//        tagDTOForSelectTwo = new TagDTO(2, "tag-test02");
//
//        tagsList = new HashSet<>();
//        tagsList.add(tagDTOForSelectOne);
//        tagsList.add(tagDTOForSelectTwo);
//
//        tagDTONewForSave = new TagDTO("newTagForTest");
//        tagsListNew = new HashSet<>();
//        tagsListNew.add(tagDTONewForSave);
//
//        newsDTOForSelect = new NewsDTO(1, "Title-test01", "Short text in test01",
//                "Some text of news in test01", LocalDate.of(2020, 01, 28),
//                LocalDate.of(2020, 01, 28), authorDTOForSelect, tagsList);
//
//        newsDTOForSuccessSaveWithOldAuthorAndTags = new NewsDTO("Title-save", "Short text in test01",
//                "Some text of news in test01", authorDTOForSelect, tagsList);
//
//        newsDTOForSuccessSaveWithNewAuthorAndOldTags = new NewsDTO("Title-save", "Short text in test01",
//                "Some text of news in tes", authorDTOForSaveNew, tagsList);
//
//
//        newsDTOForSuccessSaveWithNewAuthorAndNewTags = new NewsDTO("Title-save", "Short text in test01",
//                "Some text of news in test", authorDTOForSaveNew, tagsListNew);
//        newsDTOForUpdate = new NewsDTO(1, "UPDATE TITTLE", "Short text in test01",
//                "Some text of news in test01", LocalDate.of(2020, 01, 28),
//                authorDTOForSelect, tagsList);
//
//        searchCriteriaList = new ArrayList<>();
//        searchCriteriaList.add(new SearchCriteria("author_name", ":", "Name-test01"));
//
//        sortCriteriaList = new ArrayList<>();
//        sortCriteriaList.add(new SortCriteria("author_surname", "asc"));
//    }
//
//    @Test
//    public void testFindOne() {
//        NewsDTO newsByIdDto = newsService.findOne(1L);
//        assertEquals(newsDTOForSelect, newsByIdDto);
//    }


//    @Test
//    public void testSaveSuccessWithOldTagsAndAuthor() {
//        newsService.save(newsDTOForSuccessSaveWithOldAuthorAndTags);
//        List<NewsDTO> allNewsList = newsService.findAll();
//        int allNewsListAfterSaveSize = allNewsList.size();
//        int listSizeAfterSaveExpected = 27;
//        assertEquals(listSizeAfterSaveExpected, allNewsListAfterSaveSize);
//    }


//    @Test
//    public void testSaveSuccessWithOldTagsAndNewAuthor() {
//        newsService.save(newsDTOForSuccessSaveWithNewAuthorAndOldTags);
//        List<NewsDTO> allNewsList = newsService.findAll();
//        int allNewsListAfterSaveSize = allNewsList.size();
//        int listSizeAfterSaveExpected = 27;
//
//        assertEquals(listSizeAfterSaveExpected, allNewsListAfterSaveSize);
//    }

//    @Test
//    public void testSaveSuccessWithNewTagsAndNewAuthor() {
//        newsService.save(newsDTOForSuccessSaveWithNewAuthorAndNewTags);
//        List<NewsDTO> allNewsList = newsService.findAll();
//        int allNewsListAfterSaveSize = allNewsList.size();
//        int listSizeAfterSaveExpected = 27;
//        assertEquals(listSizeAfterSaveExpected, allNewsListAfterSaveSize);
//    }

//    @Test
//    public void testDelete() {
//        newsService.delete(1L);
//        List<NewsDTO> allNewsListAfterDelete = newsService.findAll();
//        int allNewsListAfterDeleteSize = allNewsListAfterDelete.size();
//        int listSizeAfterDeleteExpected = 25;
//        Assert.assertEquals(listSizeAfterDeleteExpected, allNewsListAfterDeleteSize);
//    }
//
//    @Test
//    public void testUpdate() {
//        NewsDTO beforeUpdate = newsService.findOne(1L);
//        NewsDTO afterUpdate = newsService.update(newsDTOForUpdate);
//        String tittleAfterUpdate = "UPDATE TITTLE";
//        assertNotEquals(beforeUpdate, afterUpdate);
//        assertEquals(tittleAfterUpdate, afterUpdate.getTitle());
//
//    }
//
//    @Test
//    public void testFindAll() {
//        List<NewsDTO> allNewsList = newsService.findAll();
//        int allNewsListAfterDeleteSize = allNewsList.size();
//        int listSizeAfterDeleteExpected = 26;
//        Assert.assertEquals(listSizeAfterDeleteExpected, allNewsListAfterDeleteSize);
//    }
//
//    @Test
//    public void find() {
//        List<NewsDTO> newsDTOList = newsService.find(searchCriteriaList, sortCriteriaList);
//        int actualSize = newsDTOList.size();
//        int expectedSize = 4;
//        assertEquals(expectedSize, actualSize);
//        String surnameActualAfterSort = newsDTOList.get(3).getAuthorDTO().getSurname();
//        String surnameExpectedAfterSort = "Surname-test02";
//        assertEquals(surnameExpectedAfterSort, surnameActualAfterSort);
//    }
//    @Test
//    public void findAllWithSort() {
//        List<NewsDTO> newsDTOList = newsService.findAllWithSort(sortCriteriaList);
//        String surnameActualAfterSort = newsDTOList.get(25).getAuthorDTO().getSurname();
//        String surnameExpectedAfterSort = "Surname-test20";
//        assertEquals(surnameExpectedAfterSort, surnameActualAfterSort);
//    }

}