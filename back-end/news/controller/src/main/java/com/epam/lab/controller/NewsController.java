package com.epam.lab.controller;

import com.epam.lab.dto.NewsDTO;
import com.epam.lab.service.NewsService;
import com.epam.lab.specification.SearchCriteria;
import com.epam.lab.specification.SortCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/news")
@Validated
@CrossOrigin(origins = { "http://localhost:3000"})
public class NewsController {
    private NewsService newsService;

    private static final String BAD_FORMAT_ID = "Id must be only positive number";
    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @GetMapping(value = "/{id}")
    public NewsDTO findNews(@PathVariable("id") @Positive (message = BAD_FORMAT_ID) Long id,
                            HttpServletResponse response) {
        return newsService.findOne(id);
    }

    @GetMapping
    @ResponseBody
    public List<NewsDTO> search(@RequestParam(value = "search", required = false) String search,
                                @RequestParam(value = "sort", required = false) String sort) {
        List<SearchCriteria> searchParams = new ArrayList<>();
        List<SortCriteria> sortParams = new ArrayList<>();
        if (search != null) {
            Pattern pattern = Pattern.compile("(\\w+?)([:<>])(\\w+?),");
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                searchParams.add(new SearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3)));
            }
        }
        if (sort != null) {
            Pattern pattern = Pattern.compile("(\\w+?)(:)(\\w+),");
            Matcher matcher = pattern.matcher(sort + ",");
            while (matcher.find()) {
                sortParams.add(new SortCriteria(matcher.group(1), matcher.group(3)));
            }
        }
        return newsService.find(searchParams, sortParams);
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public NewsDTO addNews(@Valid @RequestBody final NewsDTO model, HttpServletRequest request,
                           HttpServletResponse response) {

        NewsDTO newsDTO = newsService.save(model);
        long id = newsDTO.getId();
        String uri = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
                + request.getContextPath() + request.getServletPath() + "/" + id;
        response.addHeader("Location", uri);
        return newsDTO;
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteNews(@PathVariable("id") @Positive(message = BAD_FORMAT_ID) Long id) {
        newsService.delete(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/{id}")
    public NewsDTO updateNews(@Valid @RequestBody final NewsDTO model, HttpServletRequest request,
                              HttpServletResponse response) {
        return newsService.update(model);
    }

}
