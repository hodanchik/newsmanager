package com.epam.lab.configuration;

import org.springframework.core.annotation.Order;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;

import javax.servlet.ServletRegistration;
@Order(1)
public class WebAppInitializer implements WebApplicationInitializer {
        private static final int LOAD_ON_START_UP = 1;
    private static final String SERVLET_NAME = "app";
    private static final String SERVLET_MAPPING = "/*";
    private static final String SPRING_PROFILES_ACTIVE = "spring.profiles.active";
    private static final String PRODUCTION_PROFILE = "prod";
    private AnnotationConfigWebApplicationContext rootContext;

    public void onStartup(ServletContext servletContext) {
        initAppContext(servletContext);
        initServlet(servletContext);
    }
    private void initAppContext(final ServletContext servletContext) {
        rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(ControllerConfiguration.class);
        rootContext.setServletContext(servletContext);
        servletContext.setInitParameter(SPRING_PROFILES_ACTIVE, PRODUCTION_PROFILE);
        rootContext.refresh();
    }

    private void initServlet(final ServletContext servletContext) {
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet(SERVLET_NAME, new DispatcherServlet(rootContext));
        dispatcher.setLoadOnStartup(LOAD_ON_START_UP);
        dispatcher.addMapping(SERVLET_MAPPING);
    }
}
