package com.epam.lab.controller;

import com.epam.lab.dto.AuthorDTO;
import com.epam.lab.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@RequestMapping("/api/authors")
@Validated
@CrossOrigin(origins = { "http://localhost:3000"})
public class AuthorController {

    private static final String BAD_FORMAT_ID = "Id must be only positive number";
    private AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping(value = "/{id}")
    public AuthorDTO findAuthor(@PathVariable("id") @Positive (message = BAD_FORMAT_ID) Long id) {
        return authorService.findOne(id);
    }

    @GetMapping
    public List<AuthorDTO> findAllAuthor() {
        return authorService.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AuthorDTO addAuthor(@Valid @RequestBody final AuthorDTO model, HttpServletRequest request,
                               HttpServletResponse response) {

        AuthorDTO saveAuthorDTO = authorService.save(model);
        long id = saveAuthorDTO.getId();
        String uri = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
                + request.getContextPath() + request.getServletPath() + "/" + id;
        response.addHeader("Location", uri);
        return saveAuthorDTO;
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAuthor(@PathVariable("id") @Positive (message = BAD_FORMAT_ID) Long id) {
        authorService.delete(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/{id}")
    public AuthorDTO updateAuthor(@Valid @RequestBody final AuthorDTO model, HttpServletRequest request,
                                  HttpServletResponse response) {
        return authorService.update(model);

    }
}


