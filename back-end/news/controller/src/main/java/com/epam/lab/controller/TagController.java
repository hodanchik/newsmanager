package com.epam.lab.controller;

import com.epam.lab.dto.TagDTO;
import com.epam.lab.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@RequestMapping("/api/tag")
@Validated
@CrossOrigin(origins = { "http://localhost:3000"})
public class TagController {
    private static final String BAD_FORMAT_ID = "Id must be only positive number";
    private TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping(value = "/{id}")
    public TagDTO findTag(@PathVariable("id") @Positive(message = BAD_FORMAT_ID) Long id,
                          HttpServletResponse response) {
        return tagService.findOne(id);
    }

    @GetMapping
    public List<TagDTO> findAllTag(HttpServletResponse response) {
        return tagService.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public TagDTO addTag(@Valid @RequestBody TagDTO model, HttpServletRequest request,
                         HttpServletResponse response) {
        TagDTO saveTagDTO = tagService.save(model);
        long id = saveTagDTO.getId();
        String uri = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
                + request.getContextPath() + request.getServletPath() + "/" + id;
        response.addHeader("Location", uri);
        return saveTagDTO;
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteTag(@PathVariable("id") @Positive(message = BAD_FORMAT_ID) Long id, HttpServletResponse response) {
        tagService.delete(id);
    }

   @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/{id}")
    public TagDTO updateTag(@Valid @RequestBody final TagDTO model, HttpServletResponse response) {
        return tagService.update(model);
    }

}
