package com.epam.lab.controller;

import com.epam.lab.dto.AuthorDTO;
import com.epam.lab.dto.UserDTO;
import com.epam.lab.service.AuthorService;
import com.epam.lab.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@RequestMapping("/api/users")
@Validated
@CrossOrigin(origins = { "http://localhost:3000"})
public class UserController {

    private static final String BAD_FORMAT_ID = "Id must be only positive number";
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/{id}")
    public UserDTO findUser(@PathVariable("id") @Positive(message = BAD_FORMAT_ID) Long id) {
        return userService.findOne(id);
    }

    @GetMapping
    public List<UserDTO> findAllUsers() {
        return userService.findAll();
    }

//    @PostMapping(value = "/checkUser")
//    public UserDTO loginUser( @RequestBody final UserDTO model, HttpServletRequest request,
//                              HttpServletResponse response) {
//        return userService.login(model);
//    }


//    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED)
//    public UserDTO addUser(@Valid @RequestBody final UserDTO model, HttpServletRequest request,
//                               HttpServletResponse response) {
//
//        UserDTO saveUserDTO = userService.save(model);
//        long id = saveUserDTO.getId();
//        String uri = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
//                + request.getContextPath() + request.getServletPath() + "/" + id;
//        response.addHeader("Location", uri);
//        return saveUserDTO;
//    }

//    @DeleteMapping(value = "/{id}")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void deleteUser(@PathVariable("id") @Positive (message = BAD_FORMAT_ID) Long id) {
//        userService.delete(id);
//    }

//    @ResponseStatus(HttpStatus.OK)
//    @PutMapping(value = "/{id}")
//    public UserDTO updateUser(@Valid @RequestBody final UserDTO model, HttpServletRequest request,
//                                  HttpServletResponse response) {
//        return userService.update(model);
//
//    }
}
