import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../node_modules/bootstrap/dist/js/bootstrap.min.js'

import {I18nextProvider} from 'react-i18next';
import i18next from 'i18next';
import common_ru from "./translations/ru/common.json";
import common_en from "./translations/en/common.json";
import LanguageDetector from 'i18next-browser-languagedetector';


const options = {
    order: ['cookie'],
    lookupCookie: 'lng',
};
i18next
    .use(LanguageDetector)
    .init({
        interpolation: { escapeValue: false },
        detection: options,
        fallbackLng: "en",     
        defaultNS: 'default',
        resources: {
            en: {
                default: common_en
            },
            ru: {
                default: common_ru
            },
        },
    });


ReactDOM.render(
    <I18nextProvider i18n={i18next}>
        <App />
    </I18nextProvider>,
    document.getElementById('root')
);
