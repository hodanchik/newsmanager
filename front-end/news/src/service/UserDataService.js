import axios from 'axios'

const USER_API_URL = 'http://localhost:8081/newsManager/user';

class UserDataService {

    retrieveAllUsers() {
        return axios.get(`${USER_API_URL}`);
    }

    deleteUser(id) {
        return axios.delete(`${USER_API_URL}/${id}`);
    }

    retrieveUser(id) {
        return axios.get(`${USER_API_URL}/${id}`);
    }

    updateUser(id, user) {
        return axios.put(`${USER_API_URL}/${id}`, user);

    }
    createUserr(user) {
        console.log('create')
        return axios.post(`${USER_API_URL}`, user);
    }


    loginUser(user) {
       return axios.post(`${USER_API_URL}/checkUser`, user);
    }


}

export default new UserDataService()
