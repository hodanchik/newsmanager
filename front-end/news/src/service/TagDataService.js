import axios from 'axios'

const TAG_API_URL = 'http://localhost:8081/newsManager/tag';

class TagDataService {

    retrieveAllTag() {
        return axios.get(`${TAG_API_URL}`);
    }

    deleteTag(id) {
        return axios.delete(`${TAG_API_URL}/${id}`);
    }

    retrieveTag(id) {
        return axios.get(`${TAG_API_URL}/${id}`);
    }

    updateTag(id,tag) {
         return axios.put(`${TAG_API_URL}/${id}`, tag);
        
    }
    createTag(tag) {
        console.log('create')
        return axios.post(`${TAG_API_URL}`, tag);
    }
}

export default new TagDataService()
