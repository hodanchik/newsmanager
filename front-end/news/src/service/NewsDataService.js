import axios from 'axios'

const NEWS_API_URL = 'http://localhost:8081/newsManager/news';

class NewsDataService {

    retrieveAllNews() {
        return axios.get(`${NEWS_API_URL}`);
    }

    deleteNews(id) {
        return axios.delete(`${NEWS_API_URL}/${id}`);
    }

    retrieveNews(id) {
        return axios.get(`${NEWS_API_URL}/${id}`);
    }

    updateNews(id,news) {
         return axios.put(`${NEWS_API_URL}/${id}`, news);
        
    }
    createNews(news) {
        console.log('create')
        return axios.post(`${NEWS_API_URL}`, news);
    }
}

export default new NewsDataService()
