import axios from 'axios'

const AUTHOR_API_URL = 'http://localhost:8081/newsManager/authors';

class AuthorDataService {

    retrieveAllAuthors() {
        return axios.get(`${AUTHOR_API_URL}`);
    }

    deleteAuthor(id) {
        return axios.delete(`${AUTHOR_API_URL}/${id}`);
    }

    retrieveAuthor(id) {
        return axios.get(`${AUTHOR_API_URL}/${id}`);
    }

    updateAuthor(id,author) {
         return axios.put(`${AUTHOR_API_URL}/${id}`, author);
        
    }
    createAuthor(author) {
        return axios.post(`${AUTHOR_API_URL}`, author);
    }
}

export default new AuthorDataService()
