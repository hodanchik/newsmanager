import React, { Component } from 'react';
import './App.css';
import InstructorApp from './component/InstructorApp';
import { withTranslation } from 'react-i18next';
class App extends Component {
  
 
  render() { 
    
    return (
      <div className="container">
                <InstructorApp />
      </div>
    );
  }
}
export default withTranslation()(App);