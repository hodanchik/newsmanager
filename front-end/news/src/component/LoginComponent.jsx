import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import UserDataService from '../service/UserDataService';
import { withTranslation } from 'react-i18next';
class LoginComponent extends Component {


    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: ''
        }
        localStorage.removeItem('userName');
        this.onSubmit = this.onSubmit.bind(this)
    }

    onSubmit(values) {
        console.log(values);
        let user = {
            login: values.login,
            password: values.password
        }
        console.log(user);
        UserDataService.loginUser(user)
            .then(
                response => {
                    console.log(response);
                    user.id = response.data.id;
                    console.log("user.id" + user.id);
                    user.name = response.data.name;
                    user.surname = response.data.surname;
                    if (user.id == 1) {
                        console.log("user.id" + user.id);
                        localStorage.setItem('userName', user.name)
                        // this.props.history.push('/news')
                        window.location.href = "/news";
                    }
                    else {
                        localStorage.removeItem('userName')
                        // this.props.history.push('/login')
                        window.location.href = "/login";
                    }
                }
            )

    }

    render() {
        localStorage.removeItem('userName')
        let { login, password } = this.state
        return (
            <div>
                <h3>LOGIN</h3>
                <div className="container">
                    <Formik
                        initialValues={{ login, password }}
                        onSubmit={this.onSubmit}
                        validateOnChange={false}
                        validateOnBlur={false}
                        validate={this.validate}
                        enableReinitialize={true}

                    >
                        {
                            (props) => (
                                <Form>
                                    <ErrorMessage name="name" component="div"
                                        className="alert alert-warning" />
                                    <fieldset className="form-group">
                                        <label>{this.props.t("Field.login")}</label>
                                        <Field className="form-control" type="text" name="login" />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>{this.props.t("Field.password")}</label>
                                        <Field className="form-control" type="password" name="password" />
                                    </fieldset>
                                    <button className="btn btn-success" type="submit">{this.props.t("Button.enter")}</button>
                                </Form>
                            )
                        }
                    </Formik>

                </div>
            </div>
        )
    }
}


export default withTranslation()( LoginComponent);