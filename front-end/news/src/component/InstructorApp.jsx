import React, { Component } from 'react';
import ListTagsComponent from './ListTagsComponent';
import ListNewsComponent from './ListNewsComponent';
import TagComponent from './TagComponent';
import NewsComponent from './NewsComponent';
import ListAuthorsComponent from './ListAuthorsComponent';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Header from './Header';
import Footer from './Footer';
import AuthorComponent from './AuthorComponent';
import LoginComponent from './LoginComponent';
import NewsViewComponent from './NewsViewComponent';
class InstructorApp extends Component {
    render() {
        return (


            <Router>
                <><div className="container">
                    <Header />
                </div>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <Switch>
                        <Route path="/login" exact component={LoginComponent} />
                        <Route path="/tag" exact component={ListTagsComponent} />
                        <Route path="/tag/:id" exact component={TagComponent} />
                        <Route path="/news" exact component={ListNewsComponent} />
                        <Route path="/news/:id" exact component={NewsComponent} />
                        <Route path="/news/view/:id" exact component={NewsViewComponent} />
                        <Route path="/authors" exact component={ListAuthorsComponent} />
                        <Route path="/authors/:id" exact component={AuthorComponent} />
                    </Switch>
                </>
                <br></br><br></br><br></br>
                <div className="container">
                    <Footer />
                </div>
            </Router>
        )
    }
}

export default InstructorApp