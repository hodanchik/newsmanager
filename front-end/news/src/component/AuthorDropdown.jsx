import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import AuthorDataService from '../service/AuthorDataService';


class AuthorDropdown extends Component {

    constructor(props) {
        super(props)
        this.state = {
            authors: []
        }
        this.refreshAuthor = this.refreshAuthor.bind(this);
    }
    componentDidMount() {
        this.refreshAuthor();
    }

    refreshAuthor() {
        AuthorDataService.retrieveAllAuthors()
            .then(
                response => {
                    this.setState({ authors: response.data })
                }
            )
    }
    render() {
        const authorOptions = this.state.authors.map(authorDTO => {
            return (<option value={authorDTO.id} key={authorDTO.id} >
                {authorDTO.name} {authorDTO.surname}</option >
            )

        }
        );
        return (
        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
            <fieldset className="form-group">
                <Field name="authorSelect" component="select" className="form-control" >
                    {authorOptions}
                </Field>
            </fieldset>
        </div>
        )



    }
}
export default AuthorDropdown
