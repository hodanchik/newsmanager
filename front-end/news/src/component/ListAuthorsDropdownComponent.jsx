import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import AuthorDataService from '../service/AuthorDataService';


class AuthorNewsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            authors: [],
            selectedAuthorId: props.authorId
        }
        this.refreshAuthor = this.refreshAuthor.bind(this);


    }

    componentDidMount() {
        this.refreshAuthor();
    }

    refreshAuthor() {
        AuthorDataService.retrieveAllAuthors()
            .then(
                response => {
                    this.setState({ authors: response.data })
                }
            )
    }

    render() {
        const authorOptions = this.state.authors.map(authorDTO => {
            if (authorDTO.id === this.props.authorId) {
                return (
                    <option value={authorDTO.id} key={authorDTO.id} selected>
                        {authorDTO.name} {authorDTO.surname}</option>
                )
            }
            else {
                return (<option value={authorDTO.id} key={authorDTO.id} >
                    {authorDTO.name} {authorDTO.surname}</option >
                )

            }
        });




        return (



            <Field name="authorSelect" component="select" >
                
         {authorOptions}
            
            </Field>




            // <Field name="authorDTO" component="select" >
            //     {
            //         this.state.authors.map(authorDTO =>
            //             <option value={authorDTO.id} key={authorDTO.id}>
            //                 {authorDTO.name}{authorDTO.surname}</option>)
            //     }
            // </Field>

        )

    }
}
export default AuthorNewsComponent

