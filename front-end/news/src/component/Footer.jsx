import React, { Component } from 'react';

class Footer extends Component {
	render(){
		return(
			<footer class="footer font-small bg-primary justify-content-center fixed-bottom">
				<div class="footer-copyright text-center py-3 text-light ">
					© Copyright EPAM 2020: All right reserved
				</div>
			</footer>
		);
	}
}

export default Footer