import React, { Component, useState, useEffect } from 'react';
import { Formik, Form, Field, FieldArray, ErrorMessage } from 'formik';
import NewsDataService from '../service/NewsDataService';
import AuthorDropdown from './AuthorDropdown';
import TagMultiselect from './TagMultiselect';
import Pagination from './Pagination';
import { withTranslation } from 'react-i18next';
class ListNewsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            newsList: [],
            message: null,
            pageOfItems: []

        }
        this.refreshNews = this.refreshNews.bind(this);
        this.onSubmit = this.onSubmit.bind(this)
        this.onChangePage = this.onChangePage.bind(this);
    }

    onChangePage(pageOfItems) {
        this.setState({ pageOfItems: pageOfItems });
    }

    componentDidMount() {
        this.refreshNews();
    }

    refreshNews() {
        NewsDataService.retrieveAllNews()
            .then(
                response => {
                    console.log(response);
                    this.setState({ newsList: response.data })

                }
            )
    }

    deleteNewsClicked(id) {
        NewsDataService.deleteNews(id)
            .then(
                response => {
                    this.setState({ message: `Delete of news ${id} Successful` })
                    this.refreshNews()
                }
            )
    }


    onSubmit(values) {

        const tagForSearch = [];
        let selected = this.tagMultiselectRef.current.getSelectedItems();
        for (let i of selected) {
            console.log(i.name)
            tagForSearch.push({
                id: i.id,
                name: i.name
            })
        }
        let selectedAuthorId;
        if (values.authorSelect) {
            selectedAuthorId = values.authorSelect
        }
    }
    render() {
        let tagsSet = [];
        return (
            <div className="container">

                <div class="row">
                    <div class="col-2">
                        <div className="container" name="testName2" id="testId2">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link active" href="/authors">{this.props.t("Links.addEditAuthor")}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/news/0">{this.props.t("Links.addNews")}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href='/tag'>{this.props.t("Links.addEditTags")}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-10">

                        <div className="container" width="300px">
                            <Formik
                                initialValues={{}}
                                onSubmit={this.onSubmit}
                                validateOnChange={false}
                                validateOnBlur={false}
                                validate={this.validate}
                                enableReinitialize={true}
                            >
                                <Form>
                                    <AuthorDropdown />
                                    <TagMultiselect ref={this.tagMultiselectRef} tagsSet={tagsSet} />
                                    <button className="btn btn-success" type="submit">{this.props.t("Button.find")}</button>
                                </Form>
                            </Formik>
                            {this.state.pageOfItems.map(
                                news =>
                                    <tr key={news.id}>
                                        <div class="container">
                                            <div class="card" style={{ width: '50rem', height: '15rem' }}>
                                                <div class="card-body">
                                                    <h3 class="card-title"> {news.title}    </h3>
                                                    <div class="row" name="author_date">
                                                        <div class="col-md-4">
                                                            <h6 class="card-subtitle mb-2 text-muted">
                                                                {news.authorDTO.name} {news.authorDTO.surname}
                                                            </h6>
                                                        </div>
                                                        <div class="col-md-2 offset-md-6">
                                                            <h6 class="card-subtitle mb-2 text-muted">
                                                                {news.creationDate}
                                                            </h6>
                                                        </div>
                                                    </div>
                                                    <p class="card-text">{news.shortText}</p>
                                                    <h6 class="card-subtitle mb-2 text-muted">{news.tagsSet.map(tag => '#' + tag.name)}</h6>
                                                </div>
                                                <br />
                                                <br />
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <button type="button" class="btn btn-danger" onClick={() => this.deleteNewsClicked(news.id)}>{this.props.t("Button.delete")}</button>
                                                    </div>
                                                    <div class="col-md-0.2 offset-md-1"> <button type="button" class="btn btn-warning" onClick={() => this.props.history.push(`/news/${news.id}`)}>{this.props.t("Button.edit")}</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </tr>)}

                            <div>
                                <Pagination items={this.state.newsList} onChangePage={this.onChangePage} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
export default withTranslation()(ListNewsComponent);
