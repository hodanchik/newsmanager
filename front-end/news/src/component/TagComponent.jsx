import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import TagDataService from '../service/TagDataService';

class TagComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            name: ''
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)

    }

    componentDidMount() {
        // eslint-disable-next-line
        if (this.state.id == 0) {
            return
        }
        TagDataService.retrieveTag(this.state.id)
            .then(response =>{ 
                console.log(response)
            })
            //      this.setState({
            //     name: response.data.name
            // }))
                    console.log("MOUNT TAG after this.state.name: " + this.state.name)
            console.log("MOUNT TAG after this.state.id: " + this.state.id)
    }


    validate(values) {
        let errors = {}
        if (!values.name) {
            errors.name = 'Enter a Name'
        } else if (values.name.length > 30) {
            errors.name = 'Enter name less then 30 Characters '
        }

        return errors
    }
    onSubmit(values) {
        console.log(values);
        let tag = {
            id: values.id,
            name: values.name
            // targetDate: values.targetDate
        }
        if (this.state.id < 1) {
            let tag = {
                name: values.name
            }
            TagDataService.createTag(tag)
                .then(() => this.props.history.push('/tag'))
        } else {
            TagDataService.updateTag(this.state.id, tag)
                .then(() => this.props.history.push('/tag'))
        }

    }

    render() {

        let { id, name } = this.state
        return (
            <div>
                <h3>TAG</h3>
                <div className="container">
                    <Formik
                        initialValues={{ id, name }}
                        onSubmit={this.onSubmit}
                        validateOnChange={false}
                        validateOnBlur={false}
                        validate={this.validate}
                        enableReinitialize={true}
                    >
                        {
                            (props) => (
                                <Form>
                                    <ErrorMessage name="name" component="div"
                                        className="alert alert-warning" />
                                    <fieldset className="form-group">
                                        <label>Id</label>
                                        <Field className="form-control" type="text" name="id" disabled />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>Name</label>
                                        <Field className="form-control" type="text" name="name" />

                                    </fieldset>
                                    <button className="btn btn-success" type="submit">Save</button>
                                </Form>
                            )
                        }
                    </Formik>

                </div>
            </div>
        )
    }
}

export default TagComponent