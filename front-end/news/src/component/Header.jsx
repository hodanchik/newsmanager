import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

class Header extends Component {
   
   

    logOutAction(){
        localStorage.removeItem('userName');
        window.location.href="/login";
    }

    changeLanguage(language){
                document.cookie = "lng=" + language;
                window.location.reload(); 
        }

      
    
    
    render() {

        const name = localStorage.getItem('userName')
        let LoginPartButton;
            if (name) {
                LoginPartButton =
                <div >
                    <ul className="nav navbar-nav ml-auto w-100 justify-content-end">
                    <h10>{this.props.t("Header.hello")} {name}  </h10>
                        <li className="nav-item active">
                            <a className="nav-link"  href="#" onClick={() => this.logOutAction()} >{this.props.t("Header.logOut")}</a>
                        </li>
                  </ul>
                  </div>
            }
            else { LoginPartButton =
                <div >
                <ul className="nav navbar-nav ml-auto w-100 justify-content-end">
                    <li className="nav-item active">
                        <a className="nav-link" href="/login">{this.props.t("Header.logIn")}</a>
                    </li>
                    <li className="nav-item active">
                        <a className="nav-link" href="#">{this.props.t("Header.register")}</a>
                    </li>
                </ul>
                </div>

            }
        
return (

    <nav className="navbar navbar-expand-md navbar-dark bg-primary justify-content-center fixed-top">

        <a className="navbar-brand d-flex w-50 mr-auto" href="#"> {this.props.t("Header.project")}</a>

        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar" aria-controls="collapsingNavbar" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>

        <div className="navbar-collapse collapse w-100" id="collapsingNavbar">
            <ul className="navbar-nav w-100 justify-content-center">
                <li className="nav-item active">
                    <a className="nav-link" href="#"
                    onClick={()=>this.changeLanguage('en')}
                    >{this.props.t("Header.en")}</a>
                </li>
                <li className="nav-item active">
                    <a className="nav-link" href="#"
                      onClick={()=>this.changeLanguage('ru')}
                     >{this.props.t("Header.ru")}</a>
                </li>
            </ul>
            {LoginPartButton}
        </div>
    </nav>);
    }
}

export default withTranslation()(Header);