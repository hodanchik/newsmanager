import React, { Component } from 'react';
import { Multiselect } from 'multiselect-react-dropdown';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import TagDataService from '../service/TagDataService';
class TagMultiselect extends Component {
    constructor(props) {
        console.log("CONSTRUCTOR")
        super(props)
        this.state = {
            allTags: [],
            selectedTags: props.tagsSet
        };

    }

    componentDidMount() {
        TagDataService.retrieveAllTag()
            .then(
                response => {
                    this.setState({
                        allTags: response.data
                    })
                }
            )
    }

    render() {
        return (
            <div class="col">
              
                <fieldset className="form-group">
                    <Multiselect name="id_for_Mult"
                        options={this.state.allTags}
                        selectedValues={this.state.selectedTags}
                        ref={this.props.innerRef}
                        displayValue="name"
                    />
                </fieldset>
            </div>

        )
    }
}
export default React.forwardRef((props, ref) => <TagMultiselect
    innerRef={ref} {...props} />);
