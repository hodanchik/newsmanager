import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import NewsDataService from '../service/NewsDataService';
import AuthorNewsComponent from './AuthorNewsComponent';
import TagMultiselect from './TagMultiselect';
import { withTranslation } from 'react-i18next';
class NewsComponent extends Component {

    constructor(props) {
        super(props)
        this.tagMultiselectRef = React.createRef();
        this.state = {
            id: this.props.match.params.id,
            title: '',
            shortText: '',
            fullText: '',
            creationDate: '',
            modificationDate: '',
            authorDTO: {
                id: '',
                name: '',
                surname: ''
            },
            tagsSet: []
        }
        this.onSubmit = this.onSubmit.bind(this)

    }

    componentDidMount() {

        NewsDataService.retrieveNews(this.state.id)
            .then(response => this.setState({
                title: response.data.title,
                shortText: response.data.shortText,
                fullText: response.data.fullText,
                creationDate: response.data.creationDate,
                modificationDate: response.data.modificationDate,
                authorDTO: response.data.authorDTO,
                tagsSet: response.data.tagsSet,
            }))
    }

    onSubmit(values) {
        const tagSetAfterUpdate = [];
        let selected = this.tagMultiselectRef.current.getSelectedItems();
        for (let i of selected) {
            tagSetAfterUpdate.push({
                id: i.id,
                name: i.name
            })
        }

        let authorDTOAfterUpdate;

        if (values.authorName) {
            authorDTOAfterUpdate = {
                name: values.authorName,
                surname: values.authorSurname
            }
        }
        else if (values.authorSelect) {
            authorDTOAfterUpdate = {
                id: values.authorSelect
            }
        }
        else {
            authorDTOAfterUpdate = {
                id: this.state.authorDTO.id
            }
        }

        let news = {
            title: values.title,
            shortText: values.shortText,
            fullText: values.fullText,
            authorDTO: authorDTOAfterUpdate,
            tagsSet: tagSetAfterUpdate
        }

        if (this.state.id == 0) {
            NewsDataService.createNews(news)
                .then(() => this.props.history.push('/news'))
        }
        else {
            news.id = this.state.id;
            news.creationDate = this.state.creationDate;
            news.modificationDate = this.state.modificationDate;
            NewsDataService.updateNews(this.state.id, news)
                .then(() => this.props.history.push('/news'))
        }
    }
    render() {
        if (this.state.id > 0 && !this.state.creationDate) {
            return null;
        }
        let { title, shortText, fullText, authorDTO } = this.state;
        let authorName = "";
        let authorSurname = "";
        return (

            <div>
                <h3>{this.props.t("Field.addEditNews")} </h3>
                <div className="container" name="testName" id="testId" width="500px">
                    <Formik
                        initialValues={{ title, shortText, fullText, authorName, authorSurname }}
                        onSubmit={this.onSubmit}
                        validateOnChange={false}
                        validateOnBlur={false}
                        validate={this.validate}
                        enableReinitialize={true}
                    >
                        {
                            (props) => (
                                <Form>
                                    <ErrorMessage name="name" component="div"
                                        className="alert alert-warning" />
                                    <fieldset className="form-group">
                                        <label>{this.props.t("Field.title")}</label>
                                        <Field className="form-control" type="text" name="title" />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>{this.props.t("Field.shortText")}</label>
                                        <Field as="textarea" className="form-control" type="text" name="shortText" />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>{this.props.t("Field.fullText")}</label>
                                        <Field as="textarea" className="form-control" type="text" name="fullText" />
                                    </fieldset>

                                    <AuthorNewsComponent authorId={authorDTO.id} />
                                    <TagMultiselect ref={this.tagMultiselectRef} tagsSet={this.state.tagsSet} />
                                    <button className="btn btn-success" type="submit">{this.props.t("Button.save")}</button>
                                </Form>
                            )
                        }
                    </Formik>

                </div>
            </div>
        )
    }
}
export default withTranslation()(NewsComponent);
