import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import AuthorDataService from '../service/AuthorDataService';
import { withTranslation } from 'react-i18next';

class AuthorNewsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            authors: [],
            selectedAuthorId: props.authorId
        }
        this.refreshAuthor = this.refreshAuthor.bind(this);
    }
    componentDidMount() {
        this.refreshAuthor();
    }

    refreshAuthor() {
        AuthorDataService.retrieveAllAuthors()
            .then(
                response => {
                    this.setState({ authors: response.data })
                }
            )
    }

    render() {
        const authorOptions = this.state.authors.map(authorDTO => {
            if (authorDTO.id === this.props.authorId) {
                return (
                    <option value={authorDTO.id} key={authorDTO.id} selected>
                        {authorDTO.name} {authorDTO.surname}</option>
                )
            }
            else {
                return (<option value={authorDTO.id} key={authorDTO.id} >
                    {authorDTO.name} {authorDTO.surname}</option >
                )

            }
        });

        return (
            <div class="col">
                <h4> {this.props.t("Field.author")} </h4>
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-control="nav-home" aria-selected="true">{this.props.t("Field.existing")}</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-control="nav-profile" aria-selected="false">{this.props.t("Field.new")}</a>
                    </div>
                </nav>

                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <fieldset className="form-group">
                            <label> {this.props.t("Field.selectAuthor")} </label>
                            <Field name="authorSelect" component="select" className="form-control" >
                                {authorOptions}
                            </Field>
                        </fieldset>
                    </div>

                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <fieldset className="form-group">
                            <label> {this.props.t("Field.name")}</label>
                            <Field name="authorName" type="text" className="form-control" >
                            </Field>
                        </fieldset>
                        <fieldset className="form-group">
                            <label> {this.props.t("Field.surname")}</label>
                            <Field name="authorSurname" type="text" className="form-control" >
                            </Field>
                        </fieldset>
                    </div>

                </div>
            </div>

        )

    }
}
export default withTranslation()(AuthorNewsComponent);

