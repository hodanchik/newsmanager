import React, { Component } from 'react';
import { Formik, Form, Field, FieldArray, ErrorMessage } from 'formik';
import TagDataService from '../service/TagDataService';

class ListTagsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tags: [],
            message: null,

        }
        this.refreshTags = this.refreshTags.bind(this);
        this.updateTagClicked = this.updateTagClicked.bind(this);
        this.addTagClicked = this.addTagClicked.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    componentDidMount() {
        this.refreshTags();
    }

    refreshTags() {
        TagDataService.retrieveAllTag()
            .then(
                response => {
                    console.log(response);
                    this.setState({ tags: response.data })
                }
            )
    }

    deleteTagClicked(id) {
        TagDataService.deleteTag(id)
            .then(
                response => {
                    this.setState({ message: `Delete of tag ${id} Successful` })
                    this.refreshTags()
                }
            )

    }
    updateTagClicked(id) {
        console.log('update ' + id)
        this.props.history.push(`/tag/${id}`)
    }

    // updateTagClicked(id) {
    //     console.log('update ')
    //     return  this.props.history.push(`/tag/${id}`)


    // }
    addTagClicked() {
        this.props.history.push(`/tag/0`)
    }


    onSubmit(values) {
        console.log(values);
        let tag = {
            id: values.id,
            name: values.name
            // targetDate: values.targetDate
        }
        if (this.state.id < 1) {
            let tag = {
                name: values.name
            }
            TagDataService.createTag(tag)
                .then(() => this.props.history.push('/tag'))
        } else {
            console.log(values);
            TagDataService.updateTag(values.id, tag)
                .then(() => this.props.history.push('/tag'))
        }
    }

    // render() {

    //     const tags = this.state.tags
    //     console.log(tags)
    //     return (
    //         <Formik
    //             initialValues={{ tags }}
    //             onSubmit={this.onSubmit}
    //             validateOnChange={false}
    //             validateOnBlur={false}
    //             validate={this.validate}
    //             enableReinitialize={true} >
    //             {
    //                 (props) => (
    //                     <Form>
    //                         <FieldArray name="tags" render={arrayHelpers => (
    //                             <div>
    //                                 {tags.map((tag, index) => (
    //                                     <div key={index}>
    //                                          <td>TAG</td> 

    //                                         <Field className="form-control"  name={`tags.${index}.name`} disabled />
    //                                         <button className="btn btn-success" onClick={() => this.updateTagClicked(tag.id)}>Edit</button>




    //                              <button className="btn btn-warning" onClick={() => this.deleteTagClicked(tag.id)}>Delete</button>
    //                                     </div>
    //                                 ))}

    //                             </div>
    //                         )} />
    //                     </Form>
    //                 )
    //             }
    //         </Formik>
    //     )
    // }
    render() {

        return (
            <div className="container">
                <h3>Add/Edit Tags</h3>
                {this.state.message && <div class="alert alert-success">{this.state.message}</div>}
                <div className="container">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Delete</th>
                                <th>Update</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.tags.map(
                                    tag =>
                                        <tr key={tag.id}>
                                            <td>{tag.id}</td>
                                            <td>{tag.name}</td>
                                            <td><button className="btn btn-warning"
                                                onClick={() => this.deleteTagClicked(tag.id)}>Delete</button></td>
                                            <td><button className="btn btn-success"
                                                onClick={() => this.updateTagClicked(tag.id)}>Update</button></td>
                                        </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
                <div className="row">
                    <button className="btn btn-success" onClick={this.addTagClicked}>Add</button>
                </div>
            </div>

        )
    }
}


export default ListTagsComponent