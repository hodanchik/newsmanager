import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import NewsDataService from '../service/NewsDataService';

class NewsViewComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            title: '',
            shortText: '',
            fullText: '',
            creationDate: '',
            modificationDate: '',
            authorDTO: {
                id: '',
                name: '',
                surname: ''
            },
            tagsSet: []
        }

    }

    componentDidMount() {
        if (this.state.id == 0) {
            return
        }
        NewsDataService.retrieveNews(this.state.id)
            .then(response => this.setState({
                title: response.data.title,
                shortText: response.data.shortText,
                fullText: response.data.fullText,
                creationDate: response.data.creationDate,
                modificationDate: response.data.modificationDate,
                authorDTO: response.data.authorDTO,
                tagsSet: response.data.tagsSet,
            }))
    }

    deleteNewsClicked(id) {
        NewsDataService.deleteNews(id)
            .then(
                response => {
                    this.setState({ message: `Delete of news ${id} Successful` })
                    this.props.history.push(`/news`)
                }
            )

    }
    render() {
        const name = localStorage.getItem('userName')
        let EditPartButton;
            if ({name}) {
                EditPartButton =
                <div >
                    <button type="button" class="btn btn-danger" onClick={() => this.deleteNewsClicked(this.state.id)}>Delete</button>
                  <button type="button" class="btn btn-warning" onClick={() => this.props.history.push(`/news/${this.state.id}`)}>EDIT</button>
                  </div>
            }
        return (
            <div class="container">
                    <div class="card" style={{ width: '70rem', height: '40rem' }}>
                        <div class="card-body">
                            <div class="row" name="title_close_button">
                                <div class="col-md-4">
                                    <h2 class="card-title">
                                        {this.state.title}
                                    </h2>
                                </div>
                                <div class="col-md-2 offset-md-6">
                                <button type="button" class="btn btn-danger" onClick={() => this.props.history.push(`/news`)}> Close</button>
                                </div>
                            </div>
                            <div class="row" name="author_date">
                                <div class="col-md-4">
                                    <h6 class="card-subtitle mb-2 text-muted">
                                        {this.state.authorDTO.name} {this.state.authorDTO.surname}
                                    </h6>
                                </div>
                                <div class="col-md-2 offset-md-6">
                                <h6 class="card-subtitle mb-2 text-muted">
                                        {this.state.creationDate}
                                    </h6>
                                </div>
                            </div>
                            
                            
                            <p class="card-text">{this.state.shortText}</p>
                            <p class="card-text">{this.state.fullText}</p>
                            <h6 class="card-subtitle mb-2 text-muted">{this.state.tagsSet.map(tag => '#' + tag.name)}</h6>
                            </div>
                            {EditPartButton}
                            <br/>
                            <br/>
                            <div class="row">
                    <div class="col-auto mr-auto"><button type="button" class="btn btn-primary">Older</button></div>
                    <div class="col-auto "><button type="button" class="btn btn-primary">Newer</button></div>
                </div> 
                </div>
                 
            </div >
        )
    }
}
export default NewsViewComponent