import React, { Component } from 'react';
import { Formik, Form, Field, FieldArray, ErrorMessage } from 'formik';
import AuthorDataService from '../service/AuthorDataService';

class ListAuthorsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            authorsList: [],
            message: null,
        }
        this.refreshAuthors = this.refreshAuthors.bind(this);
        this.updateAuthorClicked = this.updateAuthorClicked.bind(this);
        this.addAuthorClicked = this.addAuthorClicked.bind(this);
        // this.onSubmit = this.onSubmit.bind(this);

    }

    componentDidMount() {
        this.refreshAuthors();
    }

    refreshAuthors() {
        AuthorDataService.retrieveAllAuthors()
            .then(
                response => {
                    this.setState({ authorsList: response.data })
                }
            )
    }



deleteAuthorClicked(id) {
    AuthorDataService.deleteAuthor(id)
        .then(
            response => {
                this.setState({ message: `Delete of author ${id} Successful` })
                this.refreshAuthors()
            }
        )

}
updateAuthorClicked(id) {
    console.log('update ' + id)
    this.props.history.push(`/authors/${id}`)
}

addAuthorClicked() {
    this.props.history.push(`/authors/0`)
}

render() {
    localStorage.setItem('test', 167676767676);
    return (
        <div className="container">
            <h3>Add/Edit Author</h3>
            {this.state.message && <div class="alert alert-success">{this.state.message}</div>}
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>Delete</th>
                            <th>Update</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.authorsList.map(
                                author =>
                                    <tr key={author.id}>
                                        <td>{author.id}</td>
                                        <td>{author.name}</td>
                                        <td>{author.surname}</td>
                                        <td><button className="btn btn-warning"
                                            onClick={() => this.deleteAuthorClicked(author.id)}>Delete</button></td>
                                        <td><button className="btn btn-success"
                                            onClick={() => this.updateAuthorClicked(author.id)}>Update</button></td>
                                    </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            <div className="row">
                <button className="btn btn-success" onClick={this.addAuthorClicked}>Add</button>
            </div>
        </div>

    )
}








}

export default ListAuthorsComponent