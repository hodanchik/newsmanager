import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import AuthorDataService from '../service/AuthorDataService';

class AuthorComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            name: '',
            surname: ''
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)

    }

    componentDidMount() {
        console.log("MOUNT AUTHOR")
        if (this.state.id == 0) {
            return
        }

        AuthorDataService.retrieveAuthor(this.state.id)
            .then(response => {
                
                console.log("response: " + response)
                console.log("response.data: " + response.data.name)
                console.log("response.id: " + response.id)
                this.setState({
                name: response.data.name,
                surname: response.data.surname
            })})
            console.log("MOUNT AUTHOR after this.state.name: " + this.state.name)
            console.log("MOUNT AUTHOR after this.state.id: " + this.state.id)
            console.log("MOUNT AUTHOR after this.state.surname: " + this.state.surname)
    }


    validate(values) {
        
        let errors = {}
        if (!values.name) {
            errors.name = 'Enter Name'
        } else if (values.name.length > 30) {
            errors.name = 'Enter name less then 30 Characters '
        }
        if (!values.surname) {
            errors.surname = 'Enter surname'
        } else if (values.surname.length > 30) {
            errors.surname = 'Enter surname less then 30 Characters '
        }


        return errors
    }
    onSubmit(values) {
        console.log(values);
        let author = {
            id: values.id,
            name: values.name,
            surname: values.surname
            
        }
        if (this.state.id < 1) {
            let author = {
                name: values.name,
            surname: values.surname
            }
            AuthorDataService.createAuthor(author)
                .then(() => this.props.history.push('/authors'))
        } else {
            AuthorDataService.updateAuthor(this.state.id, author)
                .then(() => this.props.history.push('/authors'))
        }

    }

    render() {
        console.log("RENDER AUTHOR")
        console.log( localStorage.getItem('test') );
        // if (this.state.id > 0 && !this.state.name) {
        //     console.log("RENDER AUTHOR NULL")
        //     return 
        // }
        let { id, name, surname } = this.state
        return (
            
            <div>
                <h3>AUTHOR</h3>
                <div className="container">
                    <Formik
                        initialValues={{ id, name, surname }}
                        onSubmit={this.onSubmit}
                        validateOnChange={false}
                        validateOnBlur={false}
                        validate={this.validate}
                        enableReinitialize={true}
                     
                    >
                        {
                            (props) => (
                                <Form>
                                    <ErrorMessage name="name" component="div"
                                        className="alert alert-warning" />
                                    <fieldset className="form-group">
                                        <label>Id</label>
                                        <Field className="form-control" type="text" name="id" disabled />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>Name</label>
                                        <Field className="form-control" type="text" name="name" />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>Surname</label>
                                        <Field className="form-control" type="text" name="surname" />
                                    </fieldset>
                                    <button className="btn btn-success" type="submit">Save</button>
                                </Form>
                            )
                        }
                    </Formik>
            
                </div>
            </div>
        )
    }
}

export default AuthorComponent