# Business Requirements

### Create a backend for simple News Management application.

##### The system should expose REST APIs to perform the following operations:
+ CRUD operations for News:
    + If new tags and(or) Author are passed during creation or modification of a News, then they should be created in the database. (Save news message with author and tags should go as one step in service layer.);
+  CRUD operations for Tag;
+  CRUD operations for Author;
+  Get news. All request parameters are optional and can be used in conjunction:
    +  Sort news by date or tag or author;
    +  Search according SearchCriteria (see details in Tools and Implementation Requirements section);
+   Add tag/tags for news message;
+   Add news author;
+  Each news should have only one author;
+   Each news may have more than 1 tag;

## REST API

#### Author:
+	GET: /authors - Gets all authors; 
+	GET: /authors/id - Gets author by id; 
+	POST: /authors - Persists author. Author should be sent using request-body in appropriate JSON-format;
+	PUT: /authors - Merge author. Author should be sent using request-body in appropriate JSON-format; 
+	DELETE: /authors/id - Delete author by id.

#### Tag:
+   GET: /tags - Gets all tags;
+	GET: /tags/id - Gets tag by id; 
+	PUT: /tags - Merge tag. Tag should be sent using request-body in appropriate JSON-format;
+	DELETE: /tags/id - Delete tag by id.

#### News:
*	GET: /news - Gets all news; 
*	GET: /news/id - Gets news by id; 
*	GET: /news?search=...&sort=... - Gets news according to search and sort conditions. search-conditions must be on format "tablename_fieldname" "operator" "value"  
*	Operator can be : ":" - value equals, "%"  contains data
*	sort-conditions must be on format "tablename_fieldname"":""order". * order must be "asc" for ascendence sorting and "desc" fro descendence sorting
*	GET: /news/count - Gets news quantity; 
*	POST: /news - Persists news. News should be sent using request-body in appropriate JSON-format; 
*	PUT: /news - Merge news. News should be sent using request-body in appropriate JSON-format;
*	DELETE: /news/id - Delete news by id.

## Security:
#### User Permissions:
+ Guest:
    + Read operations for News;
    + Sign up;
    + Log in;

+ User:
    + All read operations;
    + All create operations;
    + All update operations;
    + for test: login: Two password: user;

+ Administrator (can be added only via database call):
    + All operations;
    + for test: login: Admin password: admin;
